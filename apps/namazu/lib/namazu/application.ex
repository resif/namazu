defmodule Namazu.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      Namazu.Repo,
      {Namazu.Mailer, []},
      Supervisor.child_spec(
        {ConCache, [name: :forms, ttl_check_interval: 5000, global_ttl: 30_000]},
        id: :concache_forms
      ),
      Supervisor.child_spec(
        {ConCache, [name: :events, ttl_check_interval: 5000, global_ttl: 30_000]},
        id: :concache_events
      ),
      Supervisor.child_spec(
        {ConCache, [name: :misc, ttl_check_interval: 5000, global_ttl: 60_000]},
        id: :concache_misc
      ),
      Supervisor.child_spec(
        {ConCache, [name: :stations, ttl_check_interval: 60_000, global_ttl: 60_000]},
        id: :concache_stations
      ),
      Supervisor.child_spec(
        {ConCache, [name: :past_seismicity, ttl_check_interval: 60_000, global_ttl: 60_000]},
        id: :concache_past_seismicity
      ),
      Supervisor.child_spec(
        {ConCache, [name: :phases, ttl_check_interval: 60_000, global_ttl: 60_000]},
        id: :concache_phases
      ),
      Supervisor.child_spec(
        {ConCache, [name: :distintive_cities, ttl_check_interval: 5000, global_ttl: 60_000]},
        id: :concache_distintive_cities
      ),
      Supervisor.child_spec(
        {ConCache, [name: :parameters, ttl_check_interval: false]},
        id: :concache_parameters
      )
    ]

    opts = [strategy: :one_for_one, name: Namazu.Supervisor]

    Supervisor.start_link(children, opts)
  end
end
