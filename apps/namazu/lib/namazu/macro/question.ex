defmodule Namazu.Macro.Question do
  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Namazu.Macro.{QuestionChoice, QuestionCondition}
  alias Namazu.Repo

  @schema_prefix :macro

  schema "questions" do
    belongs_to(:form, Namazu.Macro.Form)
    belongs_to(:group, Namazu.Macro.QuestionGroup)

    has_many(:conditions, QuestionCondition)
    has_many(:question_choices, QuestionChoice)

    field(:answer_type, :string)
    field(:multiple, :boolean)
    field(:order, :integer)
    field(:required, :boolean)
    field(:type, :string)
    field(:text, :string)
    field(:subtext, :string)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(question, params \\ %{}) do
    question
    |> cast(params, [:form_id, :group_id, :answer_type, :multiple, :order, :required, :type, :text, :subtext])
  end

  def distinctive_fields() do
    [:form_id, :order, :text]
  end

  def get_question_with_choices(id) do
    __MODULE__
    |> join(:left, [q], c in assoc(q, :question_choices))
    |> where([q, c], q.id == ^id)
    |> preload([q, c], question_choices: c)
    |> Repo.one()
  end
end
