defmodule Mix.Tasks.Namazu.DumpMacroData do
  use Mix.Task

  import Ecto.Query

  alias Namazu.Repo
  alias Namazu.Macro.Form
  alias Namazu.Macro.Testimony

  @shortdoc "Dump macro data to a json file"

  def get_testimonies(form_id) do
    from(t in Testimony,
      left_join: e in assoc(t, :event),
      left_join: a in assoc(t, :answers),
      left_join: i in assoc(t, :intensities),
      left_join: c in assoc(a, :question_choices),
      where: t.form_id == ^form_id and i.testimony_preferred,
      preload: [event: e, answers: {a, question_choices: c}, intensities: i]
    )
  end

  def format_answer(answer) do
    %{
      value: answer.value,
      choices: Enum.map(answer.question_choices, &Map.get(&1, :question_choice_id))
    }
  end

  def format_intensity(intensity) do
    %{
      value: intensity.intensity,
      method: intensity.method,
      quality: intensity.quality,
      automatic: intensity.automatic
    }
  end

  def format_testimony(testimony) do
    %{
      event: testimony.event.publicid,
      latitude: testimony.latitude,
      longitude: testimony.longitude,
      key: testimony.key,
      quality: testimony.quality,
      answers: Enum.map(testimony.answers, &format_answer(&1)),
      intensity: Enum.map(testimony.intensities, &format_intensity(&1))
    }
  end

  def format_choice(choice) do
    %{id: choice.id, url: choice.url, value: choice.value, order: choice.order}
  end

  def format_question(question) do
    %{
      text: question.text,
      order: question.order,
      answer_type: question.answer_type,
      type: question.type,
      multiple: question.multiple,
      choices: Enum.map(question.question_choices, &format_choice(&1))
    }
  end

  def format_form(form) do
    %{
      name: form.name,
      type: form.type,
      questions: Enum.map(form.questions, &format_question(&1))
    }
  end

  def run(args) do
    Mix.Task.run("app.start")

    arguments = [form_id: :integer]

    {_options, args, _} = OptionParser.parse(args, strict: arguments)

    [form_id] = args

    form =
      form_id
      |> Form.get()
      |> Repo.one!()

    testimonies =
      form_id
      |> get_testimonies()
      |> Repo.all()

    json =
      %{
        form: format_form(form),
        testimonies: Enum.map(testimonies, &format_testimony(&1))
      }
      |> Jason.encode!()

    File.write("macro-data.json", json, [:binary])

    # |> IO.inspect(limit: :infinity)
  end
end
