CREATE SCHEMA IF NOT EXISTS instruments;

CREATE TABLE IF NOT EXISTS instruments.networks (
    id serial PRIMARY KEY,

    network_code seismic.network_code NOT NULL,
    description character varying(255),
    hidden boolean DEFAULT false,
    color character varying(7) DEFAULT '#ffffff',

    inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
    updated_at timestamp with time zone
);

CREATE TABLE IF NOT EXISTS instruments.stations (
    id serial PRIMARY KEY,
    actor_id integer REFERENCES namazu.actors,
    network_id integer NOT NULL REFERENCES instruments.networks,

    station_code seismic.station_code NOT NULL,
    description character varying(255),
    hidden boolean DEFAULT false,

    inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
    updated_at timestamp with time zone
);

CREATE TABLE IF NOT EXISTS instruments.stations_periods (
    id serial PRIMARY KEY,
    station_id integer NOT NULL REFERENCES instruments.stations,
    managed_by integer REFERENCES namazu.actors,
    latitude double precision NOT NULL,
    longitude double precision NOT NULL,

    period_start timestamp with time zone NOT NULL,
    period_end timestamp with time zone,
    period tstzrange,
    location geometry(Point, 4326),

    inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
    updated_at timestamp with time zone,

    EXCLUDE USING GIST (
        station_id WITH =,
        period WITH &&
    )
);
