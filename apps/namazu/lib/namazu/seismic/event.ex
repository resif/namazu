defmodule Namazu.Seismic.Event do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :seismic

  schema "events" do
    has_many(:origins, Namazu.Seismic.Origin)
    has_many(:magnitudes, Namazu.Seismic.Magnitude)
    belongs_to(:zone, Namazu.Zone)
    belongs_to(:distinctive_city, Namazu.Geography.City)
    belongs_to(:merged_with_event, __MODULE__)

    has_one(:preferred_origin, Namazu.Seismic.Origin, where: [event_preferred: true])
    has_one(:preferred_magnitude, Namazu.Seismic.Magnitude, where: [event_preferred: true])

    field(:publicid, :string)
    field(:event_type, :string)
    field(:localtime, :utc_datetime_usec)
    field(:timezone, :string)
    field(:distinctive_city_distance, :integer)
    field(:felt, :boolean)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:id]
  end

  def changeset(event, params \\ %{}) do
    event
    |> cast(params, [
      :event_type,
      :distinctive_city_id,
      :merged_with_event_id,
      :timezone,
      :distinctive_city_distance
    ])
  end

  def namazu_to_quakeml_event_type(event_type) do
    case event_type do
      "outside of network interest" -> "other event"
      "induced" -> "induced or triggered event"
      _ -> event_type
    end
  end

  def quakeml_to_namazu_event_type(event_type) do
    case event_type do
      "other event" -> "outside of network interest"
      "induced or triggered event" -> "induced"
      _ -> event_type
    end
  end

  def seiscomp3_to_namazu_event_type(event_type) do
    case event_type do
      "other" -> "other event"
      "induced earthquake" -> "induced"
      _ -> event_type
    end
  end

  def separate_events(events) do
    events
    |> Enum.group_by(fn e ->
      cond do
        e.preferred_origin.automatic -> :earthquakes_and_automatic_events
        e.event_type == "earthquake" -> :earthquakes_and_automatic_events
        e.event_type == "induced" -> :earthquakes_and_automatic_events
        true -> :other_events
      end
    end)
  end
end
