defmodule Web.API.V1.GeographyController do
  use Web, :controller

  import Ecto.Query
  alias Namazu.Geography.Area
  alias Namazu.Geography.Street
  # alias Namazu.Cache

  def format_city(city) do
    %{
      name: city.name,
      id: city.id,
      administrativeCode: city.administrative_code,
      administrativeCodeType: city.administrative_code_type,
      latitude: city.latitude,
      longitude: city.longitude,
      population: city.population
    }
  end

  def format_street(street) do
    %{
      name: street.name,
      latitude: street.latitude,
      longitude: street.longitude
    }
  end

  def code_valid?(code) do
    Regex.match?(~r/^\d{5}$/, code)
  end

  def cities(conn, _params = %{"latitude" => latitude, "longitude" => longitude}) do
    point = Namazu.Geography.latlng_to_geopoint(latitude, longitude)

    cities =
      Namazu.Geography.distintive_cities(point, 15)
      |> Enum.map(&format_city(&1))

    json(conn, cities)
  end

  def cities(conn, _params = %{"postal_code" => postal_code}) do
    if not code_valid?(postal_code) do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Invalid postal code : #{postal_code}"})
    end

    cities =
      Area.get_current_cities_from_postal_code(postal_code)
      |> Namazu.Repo.all()
      |> Enum.map(&format_city(&1))

    json(conn, cities)
  end

  def cities(conn, _params = %{"match" => match}) do
    cities =
      Area.get_current_cities_from_fullts(match)
      |> limit(100)
      |> Namazu.Repo.all()
      |> Enum.map(&format_city(&1))

    json(conn, cities)
  end

  def cities(conn, _params = %{"administrative_code_type" => administrative_code_type, "administrative_code" => administrative_code}) do
    cities =
      Area.get_current_cities_from_administrative_code(administrative_code_type, administrative_code)
      |> Namazu.Repo.all()
      |> Enum.map(&format_city(&1))

    json(conn, cities)
  end

  def streets(conn, _params = %{"administrative_code" => administrative_code, "match" => match}) do
    if not code_valid?(administrative_code) do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Invalid administrative_code code : #{administrative_code}"})
    end

    match = String.trim(match)

    streets =
      Street
      |> where([s], s.administrative_code == ^administrative_code)
      |> Street.get_current_streets_from_fullts(match)
      |> Namazu.Repo.all()
      |> Enum.map(&format_street(&1))

    json(conn, streets)
  end
end
