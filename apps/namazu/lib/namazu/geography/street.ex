defmodule Namazu.Geography.Street do
  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  @schema_prefix :geography

  schema "streets" do
    field(:source_id, :string)
    field(:name, :string)
    field(:postal_code, :string)
    field(:administrative_code_type, :string)
    field(:administrative_code, :string)

    field(:longitude, :float)
    field(:latitude, :float)

    field(:minimal_longitude, :float)
    field(:maximal_longitude, :float)
    field(:minimal_latitude, :float)
    field(:maximal_latitude, :float)

    field(:location, Geo.PostGIS.Geometry)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:source_id]
  end

  def changeset(country, params \\ %{}) do
    country
    |> cast(params, [
      :source_id,
      :name,
      :postal_code,
      :administrative_code_type,
      :administrative_code,
      :longitude,
      :latitude,
      :minimal_longitude,
      :maximal_longitude,
      :minimal_latitude,
      :maximal_latitude
    ])
  end

  def get_current_streets_from_fullts(query, ""), do: query

  def get_current_streets_from_fullts(query, search) do
    IO.inspect("SEarch")

    search =
      search
      |> String.split()
      |> Enum.map_join(" & ", fn s -> s <> ":*" end)
      |> IO.inspect()

    from(s in query,
      where: fragment("name_vector @@ unaccent(lower(?))::tsquery", ^search)
    )
  end
end
