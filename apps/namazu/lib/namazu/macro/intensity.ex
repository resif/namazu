defmodule Namazu.Macro.Intensity do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :macro

  schema "intensities" do
    belongs_to(:event, Namazu.Seismic.Event)
    belongs_to(:testimony, Namazu.Macro.Testimony)
    belongs_to(:city, Namazu.Geography.Area)

    field(:testimony_preferred, :boolean)
    field(:intensity, :float)
    field(:intensity_lower_uncertainty, :float)
    field(:intensity_upper_uncertainty, :float)
    field(:intensity_type, :string)
    field(:method, :string)
    field(:evaluation_status, :string)
    field(:quality, :float)
    field(:automatic, :boolean)
    field(:author, :string)
    field(:notes, :string)
    field(:city_preferred, :boolean)
    field(:city_administrative_code_type, :string)
    field(:city_administrative_code, :string)
    field(:longitude, :float)
    field(:latitude, :float)
    field(:location, Geo.PostGIS.Geometry)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [
      :event_id,
      :testimony_id,
      :city_id,
      :city_preferred,
      :city_administrative_code_type,
      :city_administrative_code,
      :intensity,
      :intensity_type,
      :method,
      :automatic,
      :latitude,
      :longitude,
      :author
    ]
  end

  def changeset(intensity, params \\ %{}) do
    intensity
    |> cast(params, [
      :event_id,
      :testimony_id,
      :city_id,
      :testimony_preferred,
      :intensity,
      :intensity_lower_uncertainty,
      :intensity_upper_uncertainty,
      :intensity_type,
      :method,
      :evaluation_status,
      :quality,
      :automatic,
      :author,
      :notes,
      :city_preferred,
      :city_administrative_code_type,
      :city_administrative_code,
      :longitude,
      :latitude,
      :location
      # :created_at
    ])
  end
end
