defmodule Namazu.Actor do
  use Ecto.Schema

  @schema_prefix :namazu

  schema "actors" do
    has_many(:stations, Namazu.Instruments.Station)

    field(:name, :string)
    field(:acronym, :string)
    field(:slug, :string)
    field(:country_iso2, :string)
    field(:url, :string)
    field(:description, :string)
  end
end
