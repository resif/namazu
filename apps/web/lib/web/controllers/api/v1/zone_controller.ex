defmodule Web.API.V1.ZoneController do
  use Web, :controller

  alias Namazu.Repo
  alias Namazu.Cache
  alias Namazu.Zone

  import Ecto.Query
  alias Namazu.Helpers

  def get_geometry(zone) do
    zone.boundary
  end

  def get_properties(zone) do
    %{
      buffer: zone.buffer,
      city_form_id: zone.city_form_id,
      individual_form_id: zone.individual_form_id,
      name: zone.name,
      show_manual: zone.show_manual,
      manual_minimal_magnitude: zone.manual_minimal_magnitude,
      manual_maximal_depth: zone.manual_maximal_depth,
      manual_minimal_used_phase_count: zone.manual_minimal_used_phase_count,
      manual_event_types: zone.manual_event_types,
      show_automatic: zone.show_automatic,
      automatic_minimal_magnitude: zone.automatic_minimal_magnitude,
      automatic_maximal_depth: zone.automatic_maximal_depth,
      automatic_minimal_used_phase_count: zone.automatic_minimal_used_phase_count,
      minimal_latitude: zone.minimal_latitude,
      maximal_latitude: zone.maximal_latitude,
      minimal_longitude: zone.minimal_longitude,
      maximal_longitude: zone.maximal_longitude
    }
  end

  def to_feature(zone) do
    Helpers.to_feature(zone, &__MODULE__.get_properties/1, &__MODULE__.get_geometry/1)
  end

  def index(conn, _params) do
    zones =
      Zone
      |> Repo.all()
      |> Enum.map(&__MODULE__.to_feature(&1))
      |> Helpers.geojson()

    json(conn, zones)
  end

  def show(conn, _params = %{"zone_slug" => zone_slug}) do
    zone =
      Zone
      |> where([z], z.slug == ^zone_slug)
      |> Repo.all()
      |> Enum.map(&__MODULE__.to_feature(&1))
      |> Helpers.geojson()

    json(conn, zone)
  end

  def stations(conn, _params = %{"zone_slug" => zone_slug}) do
    zone =
      Zone
      |> Repo.get_by!(slug: zone_slug)

    stations =
      Cache.get_stations_by_zone(zone)
      |> Enum.map(&Helpers.to_feature(&1))
      |> Helpers.geojson()

    json(conn, stations)
  end
end
