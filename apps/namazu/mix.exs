defmodule Namazu.Mixfile do
  use Mix.Project

  def project do
    [
      compilers: Mix.compilers(),
      app: :namazu,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:jason, :logger, :timex],
      mod: {Namazu.Application, []}
    ]
  end

  defp deps do
    [
      {:con_cache, "~> 0.14"},
      {:ecto_sql, "~> 3.0"},
      {:geo_postgis, "~> 2.0"},
      {:gettext, "~> 0.18"},
      {:postgrex, ">= 0.0.0"},
      {:timex, "~> 3.5"},
      {:swoosh, "~> 1.3"},
      {:gen_smtp, "~> 1.1"}
    ]
  end
end
