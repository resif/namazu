defmodule Namazu.Helpers do
  alias Namazu.Seismic.{Event}
  alias Namazu.Geography.{Area, City}
  alias Namazu.Instruments.{Station}

  require Namazu.Gettext

  def format_magnitude(magnitude), do: Float.round(magnitude, 1)

  def translate_event_type(event_type) do
    if event_type do
      Gettext.dgettext(Namazu.Gettext, "event_types", event_type)
    else
      Gettext.dgettext(Namazu.Gettext, "event_types", "event")
    end
  end

  def event_description(event) do
    event_type =
      event.event_type
      |> Namazu.Helpers.translate_event_type()
      |> String.capitalize()

    Namazu.Gettext.gettext(
      "%{event_type} of magnitude %{magnitude}, near of %{city}",
      city: event.distinctive_city.name,
      magnitude: format_magnitude(event.preferred_magnitude.magnitude),
      event_type: event_type
    )
  end

  def rounded_uncertainty(nil), do: nil

  def rounded_uncertainty(uncertainty) do
    cond do
      uncertainty < 1 -> :erlang.float_to_binary(uncertainty, decimals: 1)
      true -> Float.round(uncertainty) |> Kernel.trunc()
    end
  end

  def geojson(features) do
    %{
      type: "FeatureCollection",
      features: features
    }
  end

  def to_feature(map, get_properties_fun, get_geometry_fun) do
    geometry = get_geometry_fun.(map)

    properties = get_properties_fun.(map)

    %{
      geometry: Geo.JSON.encode!(geometry),
      properties: properties
    }
  end

  def to_feature(zone) do
    to_feature(zone, &get_properties/1, &get_geometry/1)
  end

  def get_geometry(event = %Event{}) do
    event.preferred_origin.location
  end

  def get_geometry(area = %Area{}) do
    # TODO: check period order to be sure to get the good one
    List.first(area.periods).location
  end

  def get_geometry(station = %Station{}) do
    List.first(station.periods).location
  end

  def get_properties(event = %Event{}) do
    %{
      eventID: event.id,
      time: event.preferred_origin.time,
      magnitude: event.preferred_magnitude.magnitude,
      eventType: event.event_type,
      automatic: event.preferred_origin.automatic
    }
  end

  def get_properties(station = %Station{}) do
    %{
      networkcode: station.network.network_code,
      networkcolor: station.network.color,
      stationcode: station.station_code
    }
  end

  def get_properties(area = %City{}, distance: distance) do
    %{
      areaID: area.id,
      population: area.population,
      countryISO2: area.country_iso2,
      name: area.name,
      distance: distance
    }
  end

  def get_properties(area = %Area{}, distance: distance, name: name, population: population) do
    %{
      areaID: area.id,
      population: population,
      countryISO2: area.country_iso2,
      name: name,
      distance: distance
    }
  end

  def parse_float(_value = ""), do: nil
  def parse_float(_value = nil), do: nil

  def parse_float(value) do
    {float, _} = Float.parse(value)
    float
  end

  def parse_integer(_value = ""), do: nil
  def parse_integer(_value = nil), do: nil
  def parse_integer(value), do: String.to_integer(value)

  def parse_datetime(_datetime = ""), do: nil
  def parse_datetime(_datetime = nil), do: nil

  def parse_datetime(datetime) do
    case DateTime.from_iso8601(datetime) do
      {:ok, datetime, _offset} ->
        datetime

      {:error, :missing_offset} ->
        {:ok, datetime, _offset} = DateTime.from_iso8601(datetime <> "Z")
        datetime
    end
  end

  def parse_order_by(_value = ""), do: nil
  def parse_order_by(_value = nil), do: nil

  def parse_order_by(value) do
    case value do
      "time" -> :descending_time
      "time-asc" -> :ascending_time
      "magnitude" -> :descending_magnitude
      "magnitude-asc" -> :ascending_magnitude
      "ascending_time" -> :ascending_time
      "descending_time" -> :descending_time
      "ascending_magnitude" -> :ascending_magnitude
      "descending_magnitude" -> :descending_magnitude
      _ -> nil
    end
  end

  @spec _nullify(map(), []) :: map()
  defp _nullify(map, []), do: map

  defp _nullify(map, [key | remaining_keys]) do
    case Map.get(map, key) do
      "" -> Map.put(map, key, nil)
      _ -> _nullify(map, remaining_keys)
    end
  end

  @spec nullify(map()) :: map()
  def nullify(map), do: _nullify(map, Map.keys(map))
end
