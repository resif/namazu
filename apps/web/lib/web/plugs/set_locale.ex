defmodule Web.Plugs.SetLocale do
  # import Plug.Conn

  @supported_locales Gettext.known_locales(Namazu.Gettext)

  def init(_options), do: nil

  def call(%Plug.Conn{params: %{"locale" => locale}} = conn, _default)
      when locale in @supported_locales do
    Namazu.Gettext |> Gettext.put_locale(locale)
    conn
  end

  def call(conn, _options), do: conn
end
