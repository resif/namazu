defmodule Web.Helpers do
  def filter_fields(map, fields) do
    map
    |> Enum.filter(fn {key, _value} -> key in fields end)
    |> Enum.into(%{})
  end

  def camel_case_fields(map) do
    Enum.map(map, fn {key, value} ->
      [first | remaining] = key |> Atom.to_string() |> String.split("_")

      new_key =
        (first <> (remaining |> Enum.map_join(&String.capitalize(&1))))
        |> String.to_atom()

      {new_key, value}
    end)
    |> Enum.into(%{})
  end
end
