defmodule Namazu.Repo do
  use Ecto.Repo, otp_app: :namazu, adapter: Ecto.Adapters.Postgres

  alias Namazu.Helpers

  import Ecto.Query

  def init(_type, config) do
    {:ok, config}
  end

  def overlapping_periods(query, period_start, period_end) do
    query
    |> where([p], fragment("? && tstzrange(?, ?)", p.period, ^period_start, ^period_end))
  end

  def fuzzy_search_on_name(query, name, score) do
    query
    |> where([t], fragment("similarity(?, name) > ?", ^name, ^score))
  end

  def station_periods(query, station_id) do
    query
    |> where([p], p.station_id == ^station_id)
  end

  defp build_query(query, []), do: query

  defp build_query(query, [{field, value} | remaining_fields]) do
    # if is_nil(value) and String.ends_with?(Atom.to_string(field), "_id") do
    #   raise "Can't try to match without #{field} set."
    # end

    case is_nil(value) do
      true ->
        where(query, [e], is_nil(field(e, ^field)))

      false ->
        where(query, [e], field(e, ^field) == ^value)
    end
    |> build_query(remaining_fields)
  end

  def match(map, matchable) do
    map = Helpers.nullify(map)

    fields_to_match =
      matchable.distinctive_fields()
      |> Enum.map(fn field -> {field, Map.get(map, field)} end)

    build_query(matchable, fields_to_match)
    |> Namazu.Repo.one()
  end

  def upsert(map, matchable) do
    map = Helpers.nullify(map)

    case match(map, matchable) do
      nil ->
        changeset =
          matchable
          |> struct
          |> matchable.changeset(map)

        case changeset.valid? do
          true ->
            changeset
            |> Namazu.Repo.insert()

          _ ->
            {:error, changeset}
        end

      match ->
        match
        |> matchable.changeset(map)
        |> Namazu.Repo.update()
    end
  end
end
