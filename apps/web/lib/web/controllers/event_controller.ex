defmodule Web.EventController do
  import Plug.Conn

  use Web, :controller

  require Namazu.Gettext

  import Namazu.Helpers,
    only: [
      parse_datetime: 1,
      parse_integer: 1,
      parse_order_by: 1
    ]

  alias Namazu.Repo
  alias Namazu.Seismic
  alias Namazu.Cache

  import Ecto.Query

  def fdsn_query_path(event_search, format \\ nil) do
    params = Namazu.Search.EventSearch.fdsn_query_params(event_search)

    params =
      if format do
        Map.put(params, "format", format)
      else
        params
      end
      |> Enum.map(fn {key, value} -> {key, "#{value}"} end)
      |> Enum.into(%{})

    Web.Router.Helpers.fdsn_event_path(Web.Endpoint, :query, params)
  end

  def search(conn, _params = %{"locale" => locale}) do
    event_types =
      Cache.get_event_types()
      |> Enum.filter(fn t -> t.event_type not in ["not existing", "not locatable"] end)

    title = Gettext.gettext(Namazu.Gettext, "Search")

    render(conn, "search.html", title: title, locale: locale, event_types: event_types)
  end

  def convert_start_date(start_date) do
    case start_date do
      "" -> nil
      nil -> nil
      start_date -> "#{start_date}T00:00:00Z" |> parse_datetime()
    end
  end

  def convert_end_date(end_date) do
    case end_date do
      "" -> nil
      nil -> nil
      end_date -> "#{end_date}T23:59:59.999999Z" |> parse_datetime()
    end
  end

  def search_results(conn, params = %{"locale" => locale}) do
    events_limit = 30
    query_params = Map.get(conn, :query_params)

    event_types =
      query_params
      |> Enum.filter(fn {key, value} ->
        String.starts_with?(key, "event_type_") and value == "true"
      end)
      |> Enum.map(fn {key, _value} -> String.slice(key, 11, 40) end)

    event_publicid =
      case Map.get(query_params, "event_id") do
        "" -> nil
        event_publicid -> event_publicid
      end

    search_params = %Namazu.Search.EventSearch{
      start_time: Map.get(query_params, "start_date") |> convert_start_date(),
      end_time: Map.get(query_params, "end_date") |> convert_end_date(),
      minimal_longitude:
        Map.get(query_params, "minimal_longitude") |> Namazu.Helpers.parse_float(),
      maximal_longitude:
        Map.get(query_params, "maximal_longitude") |> Namazu.Helpers.parse_float(),
      minimal_latitude: Map.get(query_params, "minimal_latitude") |> Namazu.Helpers.parse_float(),
      maximal_latitude: Map.get(query_params, "maximal_latitude") |> Namazu.Helpers.parse_float(),
      minimal_magnitude:
        Map.get(query_params, "minimal_magnitude") |> Namazu.Helpers.parse_float(),
      maximal_magnitude:
        Map.get(query_params, "maximal_magnitude") |> Namazu.Helpers.parse_float(),
      minimal_depth: Map.get(query_params, "minimal_depth") |> Namazu.Helpers.parse_float(),
      maximal_depth: Map.get(query_params, "maximal_depth") |> Namazu.Helpers.parse_float(),
      latitude: Map.get(query_params, "latitude") |> Namazu.Helpers.parse_float(),
      longitude: Map.get(query_params, "longitude") |> Namazu.Helpers.parse_float(),
      maximal_radius: Map.get(query_params, "maximal_radius") |> Namazu.Helpers.parse_float(),
      event_publicid: event_publicid,
      event_types: event_types,
      limit: Map.get(query_params, "limit") |> parse_integer(),
      offset: Map.get(query_params, "offset") |> parse_integer(),
      order_by: Map.get(query_params, "order_by") |> parse_order_by()
    }

    search_query = Namazu.Search.EventSearch.query(search_params)

    fdsn_xml_path = fdsn_query_path(search_params)

    geojson_path = fdsn_query_path(search_params, "json")

    fdsn_text_path = fdsn_query_path(search_params, "text")

    events_count = search_query |> Repo.aggregate(:count, :id)

    current_page = Map.get(params, "page", "1") |> String.to_integer()

    offset = events_limit * (current_page - 1)

    events =
      search_query
      |> Seismic.preload_distinctive_cities()
      |> offset(^offset)
      |> limit(^events_limit)
      |> Repo.all()

    title = Gettext.gettext(Namazu.Gettext, "Search results")

    event_uri_prefix = "event_uri_prefix_#{String.downcase(locale)}" |> Namazu.get_parameter()

    render(conn, "search_results.html",
      locale: locale,
      title: title,
      event_uri_domain: Namazu.get_parameter("event_uri_domain"),
      event_uri_prefix: event_uri_prefix,
      events_count: events_count,
      events_limit: events_limit,
      events: events,
      fdsn_xml_path: fdsn_xml_path,
      geojson_path: geojson_path,
      fdsn_text_path: fdsn_text_path
    )
  end
end
