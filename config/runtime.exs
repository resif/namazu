import Config

config :namazu, Namazu.Repo,
  url: System.get_env("NMZ_DATABASE", "postgres://namazu:namazu@127.0.0.1:5432/namazu")

config :seiscomp3, Seiscomp3.Repo,
  url: System.get_env("SC3_DATABASE", "postgres://postgres@127.0.0.1:5433/postgres")

config :web, Web,
  testimony_base_url: System.get_env("NMZ_TESTIMONY_BASE_URL", "http://localhost:5173")

random_secret_key =
  :crypto.strong_rand_bytes(64)
  |> Base.encode64()
  |> binary_part(0, 64)

config :web, Web.Endpoint,
  secret_key_base: System.get_env("NMZ_SECRET", random_secret_key),
  http: [
    port: System.get_env("NMZ_PORT", "4000")
  ],
  url: [
    host: System.get_env("NMZ_HOST", "localhost"),
    port: System.get_env("NMZ_PUBLIC_PORT", "443"),
    scheme: System.get_env("NMZ_SCHEME", "http")
  ]

config :namazu, Namazu.Mailer,
  adapter: Swoosh.Adapters.SMTP,
  relay: System.get_env("NMZ_SMTP_RELAY", "localhost"),
  port: System.get_env("NMZ_SMTP_PORT", "1025") |> String.to_integer(),
  ssl: false,
  tls: false,
  auth: :never
