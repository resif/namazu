defmodule Mix.Tasks.Namazu.ImportMacro do
  use Mix.Task
  use Timex

  import Ecto.Query
  import Geo.PostGIS

  require Logger

  alias Namazu.Geography
  alias Namazu.Macro.Form
  alias Namazu.Macro.Intensity
  alias Namazu.Macro.IntensityContribution
  alias Namazu.Macro.Question
  alias Namazu.Macro.QuestionChoice
  alias Namazu.Macro.QuestionCondition
  alias Namazu.Macro.QuestionConditionChoice
  alias Namazu.Macro.QuestionGroup
  alias Namazu.Macro.Testimony
  alias Namazu.Repo
  alias Namazu.Seismic

  @shortdoc "Import macro data from a JSON file"

  def parse_file(file) do
    with {:ok, binary} = File.read(file),
         {:ok, data} = Jason.decode(binary),
         do: data
  end

  def group_answers([answer | answers], map, forms_mapping, choices_mapping, old_form_id) do
    value = answer["value"]
    choice_id = forms_mapping[old_form_id][:choices_mapping][answer["question_choice_id"]]
    question_id = choices_mapping[choice_id]

    new_answer = Map.get(map, question_id, %{value: nil, choices: []})
    new_answer = %{new_answer | value: value, choices: [choice_id | new_answer.choices]}
    map = Map.put(map, question_id, new_answer)

    group_answers(answers, map, forms_mapping, choices_mapping, old_form_id)
  end

  def group_answers([], map, _forms_mapping, _choices_mapping, _old_form_id) do
    map
  end

  def cast_event(event) do
    {:ok, datetime, _} = DateTime.from_iso8601(event["time"])

    %{
      id: event["id"],
      longitude: event["longitude"],
      latitude: event["latitude"],
      magnitude: event["Ml"],
      time: datetime
    }
  end

  def schema_to_kw(event) do
    %{
      longitude: event.preferred_origin.longitude,
      latitude: event.preferred_origin.latitude,
      time: event.preferred_origin.time,
      magnitude: event.preferred_magnitude.magnitude
    }
  end

  def match_event(
        %{
          longitude: longitude,
          latitude: latitude,
          magnitude: magnitude,
          time: datetime
        },
        max_distance \\ 150
      ) do
    Seismic.event_query()
    |> Seismic.preload_preferred()
    |> where(
      [preferred_origin: po, preferred_magnitude: pm],
      ^dynamic(
        ^Seismic.start_time(Timex.shift(datetime, seconds: -80)) and
          ^Seismic.end_time(Timex.shift(datetime, seconds: 80)) and
          ^Seismic.maximal_distance(longitude, latitude, max_distance) and
          ^Seismic.minimal_magnitude(magnitude - 0.9) and
          ^Seismic.maximal_magnitude(magnitude + 0.9)
      )
    )
    |> Repo.all()
  end

  def get_events_mapping() do
    Seismic.event_query()
    |> Seismic.preload_preferred()
    |> Repo.all()
    |> Enum.map(fn e -> {e.publicid, %{id: e.id, preferred_origin: e.preferred_origin}} end)
    |> Enum.into(%{})
  end

  def import_forms(forms) do
    forms
    |> Enum.map(fn f ->
      Logger.info("Import form : #{f["name"]}")

      {:ok, form} =
        %Form{
          name: f["name"],
          type: f["type"],
          # default: f["default"]
          default: false
        }
        |> Map.from_struct()
        |> Repo.upsert(Form)

      groups_mapping =
        if Map.has_key?(f, "questions_groups") do
          f["questions_groups"]
          |> Enum.map(fn g ->
            {:ok, group} =
              %QuestionGroup{
                form_id: form.id,
                text: g["text"]
              }
              |> Map.from_struct()
              |> Repo.upsert(QuestionGroup)

            {g["id"], group.id}
          end)
          |> Map.new()
        end

      questions_mapping =
        f["questions"]
        |> Enum.map(fn q ->
          group_id =
            if Map.has_key?(q, "question_group_id"), do: groups_mapping[q["question_group_id"]]

          {:ok, question} =
            %Question{
              form_id: form.id,
              group_id: group_id,
              answer_type: q["answer_type"],
              type: q["type"],
              required: Map.get(q, "required", false),
              multiple: q["multiple"],
              order: q["order"],
              text: q["text"],
              subtext: q["subtext"]
            }
            |> Map.from_struct()
            |> Repo.upsert(Question)

          {q["id"], question.id}
        end)
        |> Map.new()

      choices_mapping =
        f["questions"]
        |> Enum.map(fn q ->
          q["question_choices"]
          |> Enum.map(fn c ->
            {:ok, choice} =
              %QuestionChoice{
                question_id: questions_mapping[q["id"]],
                url: Map.get(c, "link"),
                order: c["order"],
                value: c["value"]
              }
              |> Map.from_struct()
              |> Repo.upsert(QuestionChoice)

            {c["id"], choice.id}
          end)
        end)
        |> List.flatten()
        |> Map.new()

      f["questions"]
      |> Enum.map(fn q ->
        question_id = questions_mapping[q["id"]]

        Map.get(q, "conditions", [])
        |> Enum.map(fn c ->
          {:ok, condition} =
            %QuestionCondition{
              question_id: question_id
            }
            |> Repo.insert()

          cond do
            is_list(c["choices"]) ->
              c["choices"]

            is_number(c["choices"]) ->
              [c["choices"]]
          end
          |> Enum.map(fn choice ->
            %QuestionConditionChoice{
              question_choice_id: choices_mapping[choice],
              question_condition_id: condition.id
            }
            |> Map.from_struct()
            |> Repo.upsert(QuestionConditionChoice)
          end)
        end)
      end)

      {f["id"], %{id: form.id, choices_mapping: choices_mapping}}
    end)
    |> Map.new()
  end

  def import_testimonies(testimonies, events_mapping, forms_mapping, choices_mapping) do
    testimonies
    |> Enum.filter(fn t ->
      if t["naive_felt_time"] do
        {:ok, datetime, _offset} = DateTime.from_iso8601(t["naive_felt_time"])
        datetime.year > 1979
      else
        true
      end
    end)
    |> Enum.map(fn t ->
      Logger.info("Import testimony : #{t["key"]}")

      naive_felt_time =
        case t["naive_felt_time"] do
          nil ->
            nil

          _datetime ->
            NaiveDateTime.from_iso8601!(t["naive_felt_time"])
        end

      created_at =
        case Map.get(t, "created_at") do
          nil ->
            nil

          datetime ->
            {:ok, created_at, _offset} = DateTime.from_iso8601(datetime)
            {:ok, created_at} = Ecto.Type.cast(:utc_datetime_usec, created_at)

            created_at
        end

      old_form_id = t["form_id"]

      new_form_id =
        if old_form_id do
          forms_mapping[old_form_id].id
        else
          nil
        end

      quality =
        case t["quality"] do
          "fiable" -> 1.0
          "moyennement fiable" -> 0.5
          "peu fiable" -> 0.1
          nil -> nil
        end

      longitude = t["address"]["longitude"] / 1
      latitude = t["address"]["latitude"] / 1

      code_insee = t["extras"]["code_insee"]

      # TEMP
      # if is_nil(t["event_id"]) or Map.has_key?(events_mapping, t["event_id"]) do
      event = Map.get(events_mapping, t["event_id"])
      # IO.inspect(t["event_id"])
      # IO.inspect(event)
      event_id = if event, do: event.id
      # IO.inspect(event_id)

      time =
        if not is_nil(event) and not is_nil(Map.get(event, :preferred_origin)) do
          event.preferred_origin.time
        else
          if is_nil(naive_felt_time) do
            nil
          else
            DateTime.from_naive!(naive_felt_time, "Etc/UTC")
          end
        end

      city_id = get_city_id(longitude, latitude, time)
      # (code_insee && get_city_id(code_insee, time)) || get_city_id(longitude, latitude, time)

      postal_code =
        try do
          case Regex.run(~r/\d{5}/, t["address"]["street"]) do
            nil -> nil
            match -> List.first(match)
          end
        rescue
          _error ->
            nil
        end

      administrative_code = code_insee

      administrative_code_type =
        if code_insee do
          "INSEE"
        else
          nil
        end

      {:ok, testimony} =
        %Testimony{
          event_id: event_id,
          form_id: new_form_id,
          city_id: city_id,
          key: t["key"],
          author: t["author"],
          naive_felt_time: naive_felt_time,
          quality: quality,
          evaluation_status: "final",
          user_evaluation_status: "final",
          highlighted: false,
          address: t["address"]["street"],
          address_source: t["address"]["address_source"],
          geocoding_type: t["address"]["geocoding_type"],
          postal_code: postal_code,
          administrative_code_type: administrative_code_type,
          administrative_code: administrative_code,
          longitude: longitude,
          latitude: latitude,
          created_at: created_at,
          source: Map.get(t, "source")
        }
        |> Repo.insert(returning: true)

      # Group choices per answers
      group_answers(Map.get(t, "answers", []), %{}, forms_mapping, choices_mapping, old_form_id)
      |> Enum.each(fn {k, v} ->
        Testimony.insert_choices(k, testimony.id, v.value, v.choices)
      end)

      {t["id"], testimony.id}
    end)
    |> Enum.filter(fn t -> not is_nil(t) end)
    |> Map.new()
  end

  def convert_quality(quality) do
    case quality do
      nil -> nil
      "sûr" -> 1.0
      "moyennement sûr" -> 0.5
      "peu sûr" -> 0.1
    end
  end

  def get_city_id(longitude, latitude, time) do
    point = %Geo.Point{coordinates: {longitude, latitude}, srid: 4326}

    from(
      a in Geography.Area,
      join: ap in Geography.AreaPeriod,
      on: ap.area_id == a.id and fragment("? @> ?::timestamptz", ap.period, ^time),
      where: st_within(^point, ap.boundary) and a.area_type == "city" and not a.district,
      select: a.id
    )
    |> Repo.all()
    |> List.first()
  end

  def get_city_id(insee_code, time) do
    case insee_code do
      nil ->
        nil

      insee_code ->
        from(
          a in Geography.Area,
          join: ap in Geography.AreaPeriod,
          on: ap.area_id == a.id and fragment("? @> ?::timestamptz", ap.period, ^time),
          where:
            a.area_type == "city" and not a.district and a.administrative_code == ^insee_code and
              a.administrative_code_type == "INSEE",
          select: a.id
        )
        |> Repo.one()
    end
  end

  def import_intensity(intensity, testimonies_mapping, events_mapping) do
    latitude = Map.get(intensity, "latitude")
    longitude = Map.get(intensity, "longitude")

    testimony_id = Map.get(intensity, "testimony_id")

    event_id = Map.get(intensity, "event_id")

    code_insee = Map.get(intensity, "code_insee")

    is_nil(events_mapping[event_id]) && IO.inspect(event_id)

    time = events_mapping[event_id] && events_mapping[event_id].preferred_origin.time

    # TODO : Log when city_id is null
    city_id =
      (code_insee && get_city_id(code_insee, time)) || get_city_id(longitude, latitude, time)

    cond do
      # There are missing event_id... TOFIX
      is_nil(event_id) ->
        nil

      is_nil(events_mapping[event_id]) ->
        nil

      true ->
        Logger.info("Import intensity : #{intensity["id"]}")

        {:ok, new_intensity} =
          %{
            event_id: event_id && events_mapping[event_id].id,
            testimony_id: Map.get(testimonies_mapping, testimony_id),
            testimony_preferred: Map.get(intensity, "testimony_preferred"),
            city_id: city_id,
            intensity: Map.get(intensity, "value") || 0,
            intensity_type: Map.get(intensity, "type"),
            method: Map.get(intensity, "method"),
            # method: Map.get(intensity, "source"),
            # evaluation_status
            quality: convert_quality(Map.get(intensity, "quality")),
            automatic: Map.get(intensity, "automatic"),
            author: Map.get(intensity, "author"),
            notes: Map.get(intensity, "notes"),
            city_preferred: Map.get(intensity, "city_preferred", false),
            city_administrative_code_type: code_insee && "INSEE",
            city_administrative_code: code_insee,
            longitude: longitude,
            latitude: latitude
          }
          |> Repo.upsert(Intensity)

        {intensity["id"], new_intensity.id}
    end
  end

  def import_intensities(intensities, testimonies_mapping, events_mapping) do
    intensities
    |> Enum.map(&import_intensity(&1, testimonies_mapping, events_mapping))
    |> Enum.filter(fn i -> not is_nil(i) end)
    |> Map.new()
  end

  def import_intensities_contributions(intensities_contributions, intensities_mapping) do
    intensities_contributions
    |> Enum.filter(fn c -> not is_nil(Map.get(c, "parent_intensity_id")) end)
    |> Enum.map(fn c ->
      Logger.info("Import intensity contributions of intensity : #{c["intensity_id"]}")
      pid = Map.get(c, "parent_intensity_id")
      id = Map.get(c, "intensity_id")

      %{
        parent_intensity_id: intensities_mapping[pid],
        intensity_id: intensities_mapping[id]
      }
      |> Repo.upsert(IntensityContribution)
    end)
  end

  def get_choices_mapping() do
    QuestionChoice
    |> Repo.all()
    |> Enum.map(fn c -> {c.id, c.question_id} end)
    |> Map.new()
  end

  def run(file) do
    Mix.Task.run("app.start")

    Logger.info("Load #{file}")
    data = parse_file(file)

    Logger.info("Build events maping")
    events_mapping = get_events_mapping()

    forms_mapping = import_forms(data["forms"])

    testimonies_mapping =
      import_testimonies(
        data["testimonies"],
        events_mapping,
        forms_mapping,
        get_choices_mapping()
      )

    intensities_mapping =
      import_intensities(data["intensities"], testimonies_mapping, events_mapping)

    import_intensities_contributions(data["intensities_contributions"], intensities_mapping)

    # joined_ids =
    #   missing_events
    #   |> Enum.sort()
    #   |> Enum.map(fn id -> Integer.to_string(id) end)
    #   |> Enum.join(",")

    # Logger.info("Following #{missing_events |> length} events can't be matched : #{joined_ids}")

    :ok
  end
end
