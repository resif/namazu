#!/bin/sh

ae_dir=$1
ae_version=$(find $ae_dir -type d -name '1_DONNEES_LIVRAISON*' | rev | cut -d _ -f 1 | rev)

ogr2ogr -f GeoJSON /vsistdout/ -t_srs EPSG:4326 $(find $ae_dir -name CHEF_LIEU.shp) | jq -c """
{
  type: .type,
  name: .name,
  crs: .crs,
  features: [.features[] |
  {
    type: \"Feature\",
    geometry: {
      type: .geometry.type,
      coordinates: .geometry.coordinates
    },
    properties: {
        country_iso2: \"FR\",
        source: \"AdminExpress\",
        source_version: \"$ae_version\",
        source_id: .properties.ID,
        administrative_code_type: \"INSEE\",
        administrative_code: .properties.INSEE_COM,
        location_type: \"chef-lieu\"
    }
  }]
}
"""
