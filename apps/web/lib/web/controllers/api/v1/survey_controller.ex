defmodule Web.API.V1.SurveyController do
  import Plug.Conn

  use Web, :controller

  alias Namazu.Repo
  alias Namazu.Macro.Form
  alias Namazu.Macro.Survey
  # alias Namazu.Cache

  def forms(conn, _params) do
    forms =
      Form
      |> Repo.all()
      |> Enum.map(fn f ->
        %{
          id: f.id,
          name: f.name,
          type: f.type,
          default: f.default
        }
      end)

    json(conn, forms)
  end

  def format_choice(choice) do
    %{
      id: choice.id,
      order: choice.order,
      url: choice.url,
      value: choice.value
    }
  end

  def format_condition(condition) do
    Enum.map(condition.choices, fn c ->
      c.question_choice_id
    end)
  end

  def format_question(question) do
    %{
      id: question.id,
      groupID: question.group_id,
      answerType: question.answer_type,
      multiple: question.multiple,
      order: question.order,
      required: question.required,
      type: question.type,
      text: question.text,
      subtext: question.subtext,
      choices: Enum.map(question.question_choices, fn c -> format_choice(c) end),
      conditions: Enum.map(question.conditions, fn c -> format_condition(c) end)
    }
  end

  def format_group(group) do
    %{
      id: group.id,
      text: group.text
    }
  end

  def format_form(form) do
    %{
      id: form.id,
      name: form.name,
      type: form.type,
      default: form.default,
      groups: Enum.map(form.groups, fn g -> format_group(g) end),
      questions: Enum.map(form.questions, fn q -> format_question(q) end)
    }
  end

  def form(conn, %{"id" => id}) do
    form =
      ConCache.get_or_store(
        :forms,
        id,
        fn ->
          Form.get(id)
          |> Repo.one()
          |> format_form()
        end
      )

    json(conn, form)
  end

  def format_city_survey(survey) do
    %{
      key: survey.key,
      eventPublicID: survey.event.publicid,
      name: survey.name,
      opened: survey.opened
    }
  end

  def city_survey(conn, %{"key" => key}) do
    survey = Survey.get_by_key(key)
    json(conn, format_city_survey(survey))
  end

  # def update_city_survey(conn, params) do
  #   key = params["key"]
  #
  #   params =
  #     Map.drop(params, [
  #       "key",
  #       "timezone",
  #       "event_id",
  #       "form_id",
  #       "highlighted",
  #       "utc_offset",
  #       "quality",
  #       "source",
  #       "expert_comment",
  #       "geocoding_type"
  #     ])
  #
  #   {:ok, testimony} =
  #     Testimony
  #     |> Repo.get_by(key: key)
  #     |> Testimony.changeset(params)
  #     |> Repo.update()
  #
  #   json(conn, %{
  #     data: %{
  #       testimony: format_testimony(testimony),
  #       next_question_id: Testimony.get_next_question(testimony, 0).id
  #     }
  #   })
  # end
end
