CREATE SCHEMA IF NOT EXISTS geography;

CREATE TYPE geography.location_type AS ENUM (
  'centroid',
  'chef-lieu'
);

CREATE TYPE geography.area_type AS ENUM (
  'country',
  'city',
  'eez'
);

CREATE TABLE geography.areas (
  id serial PRIMARY KEY,

  area_type geography.area_type NOT NULL,
  district boolean NOT NULL DEFAULT false,

  geoname_id integer,
  geoname_name varchar,

  country_iso2 varchar(3),
  country_iso3 varchar(3),

  administrative_code_type varchar(10),
  administrative_code varchar(10),

  postal_codes varchar(10) [],

  source varchar(40) NOT NULL,
  source_version varchar(40) NOT NULL,
  source_id varchar(40) NOT NULL,

  created_at timestamp with time zone,
  inserted_at timestamp with time zone DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX areas_administrative_code_type_idx ON geography.areas(administrative_code_type);
CREATE INDEX areas_administrative_code_idx ON geography.areas(administrative_code);

CREATE TABLE geography.areas_periods (
  id serial PRIMARY KEY,
  area_id integer NOT NULL REFERENCES geography.areas,

  population integer,

  name varchar NOT NULL,
  name_ar varchar,
  name_bn varchar,
  name_de varchar,
  name_el varchar,
  name_en varchar,
  name_es varchar,
  name_fr varchar,
  name_hi varchar,
  name_hu varchar,
  name_id varchar,
  name_it varchar,
  name_ja varchar,
  name_ko varchar,
  name_nl varchar,
  name_pl varchar,
  name_pt varchar,
  name_ru varchar,
  name_sv varchar,
  name_tr varchar,
  name_vi varchar,
  name_zh varchar,

  latitude double precision NOT NULL,
  longitude double precision NOT NULL,
  location geometry(Point, 4326) NOT NULL,
  location_type geography.location_type NOT NULL DEFAULT 'centroid',
  boundary geometry(MultiPolygon, 4326),

  period_start timestamp with time zone,
  period_end timestamp with time zone,
  period tstzrange,

  source varchar(40) NOT NULL,
  source_version varchar(40) NOT NULL,
  source_id varchar(40) NOT NULL,

  created_at timestamp with time zone,
  inserted_at timestamp with time zone DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  EXCLUDE USING GIST (
      area_id WITH =,
      period WITH &&
  )
);

CREATE INDEX areas_periods_boundary_idx ON geography.areas_periods USING GIST(boundary);
CREATE INDEX areas_periods_location_idx ON geography.areas_periods USING GIST(location);
ALTER TABLE geography.areas_periods ADD COLUMN name_vector tsvector GENERATED ALWAYS AS (to_tsvector ('unaccent_simple', name)) STORED;
CREATE INDEX areas_periods_name_vector_idx ON geography.areas_periods USING GIN (name_vector);

-- CREATE INDEX areas_periods_name_idx ON geography.areas_periods USING GIST (name gist_trgm_ops);

CREATE TABLE IF NOT EXISTS geography.streets (
  id serial PRIMARY KEY,

  source_id varchar(40) NOT NULL,

  name varchar NOT NULL,
  postal_code varchar(10),
  administrative_code_type varchar(10),
  administrative_code varchar(10),
  latitude double precision NOT NULL,
  longitude double precision NOT NULL,
  location geometry(Point, 4326) NOT NULL,
  minimal_longitude double precision NOT NULL,
  maximal_longitude double precision NOT NULL,
  minimal_latitude double precision NOT NULL,
  maximal_latitude double precision NOT NULL,

  created_at timestamp with time zone,
  inserted_at timestamp with time zone DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX streets_administrative_code_idx ON geography.streets(administrative_code);
CREATE INDEX streets_administrative_code_type_idx ON geography.streets(administrative_code_type);
CREATE INDEX streets_location_idx ON geography.streets USING GIST(location);
CREATE INDEX streets_source_id_idx ON geography.streets(source_id);
ALTER TABLE geography.streets ADD COLUMN name_vector tsvector GENERATED ALWAYS AS (to_tsvector ('unaccent_simple', name)) STORED;
CREATE INDEX streets_name_vector_idx ON geography.streets USING GIN (name_vector);
-- CREATE INDEX streets_name_idx ON geography.streets USING GIST (name gist_trgm_ops);

CREATE TABLE IF NOT EXISTS geography.timezones (
    timezone_id varchar NOT NULL UNIQUE,
    boundary geometry(MultiPolygon, 4326) NOT NULL,

    inserted_at timestamp with time zone DEFAULT current_timestamp,
    updated_at timestamp with time zone
);

CREATE INDEX IF NOT EXISTS timezones_boundary_idx ON geography.timezones USING GIST(boundary);
