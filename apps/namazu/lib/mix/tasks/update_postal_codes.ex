defmodule Mix.Tasks.Namazu.UpdatePostalCodes do
  use Mix.Task

  alias Namazu.Geography.Area
  alias Namazu.Repo

  import Ecto.Query

  @shortdoc "Update postal codes from laposte_hexasmal.json file"

  def run(args) do
    Mix.Task.run("app.start")

    {_options, file, _} = OptionParser.parse(args, strict: [])

    File.read!(file)
    |> Jason.decode!()
    # If using GeoJSON...
    # |> Map.get("features")
    |> Enum.reduce(%{}, fn f, acc ->
      insee = f["fields"]["code_commune_insee"]
      postal_code = f["fields"]["code_postal"]

      acc = Map.put_new(acc, insee, [])

      Map.put(acc, insee, [postal_code | Map.get(acc, insee)])
    end)
    |> Enum.each(fn {insee, postal_codes} ->
      Area
      |> update(set: [postal_codes: ^postal_codes])
      |> where(administrative_code_type: "INSEE", administrative_code: ^insee)
      |> Repo.update_all([])
    end)
  end
end
