defmodule Namazu.Macro.Testimony do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Namazu.Repo
  alias Namazu.Macro.{Answer, Form, Question, AnswerQuestionChoice}

  @schema_prefix :macro

  schema "testimonies" do
    belongs_to(:form, Namazu.Macro.Form)
    belongs_to(:event, Namazu.Seismic.Event)
    belongs_to(:city, Namazu.Geography.Area)

    has_many(:answers, Namazu.Macro.Answer)
    has_many(:intensities, Namazu.Macro.Intensity)

    field(:key, :binary_id)
    field(:naive_felt_time, :naive_datetime)
    field(:felt_time, :utc_datetime_usec)
    field(:timezone, :string)
    field(:highlighted, :boolean)
    field(:utc_offset, :integer)
    field(:quality, :float)
    field(:evaluation_status, :string)
    field(:user_evaluation_status, :string)
    field(:author, :string)
    field(:email, :string)
    field(:organization, :string)
    field(:source, :string)
    field(:address, :string)
    field(:address_source, :string)
    field(:postal_code, :string)
    field(:administrative_code_type, :string)
    field(:administrative_code, :string)
    field(:expert_comment, :string)
    field(:geocoding_type)
    field(:longitude, :float)
    field(:latitude, :float)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:key]
  end

  def changeset(testimony, params \\ %{}) do
    testimony
    |> cast(params, [
      :form_id,
      :event_id,
      :city_id,
      :key,
      :felt_time,
      :naive_felt_time,
      :timezone,
      :highlighted,
      :utc_offset,
      :quality,
      :evaluation_status,
      :user_evaluation_status,
      :author,
      :email,
      :organization,
      :source,
      :address,
      :address_source,
      :postal_code,
      :administrative_code_type,
      :administrative_code,
      :expert_comment,
      :geocoding_type,
      :longitude,
      :latitude,
      :created_at
    ])
  end

  def get_answers(testimony_id) when is_number(testimony_id) do
    from(a in Answer,
      left_join: q in assoc(a, :question),
      left_join: c in assoc(a, :question_choices),
      where: a.testimony_id == ^testimony_id,
      preload: [question: q, question_choices: c]
    )
    |> Repo.all()
  end

  def get_next_question(testimony, minimal_order) do
    answers = get_answers(testimony.id)
    conditions = Form.get_conditions(testimony.form_id)

    choices =
      answers
      |> Enum.flat_map(fn a ->
        Enum.map(a.question_choices, fn c -> c.question_choice_id end)
      end)

    from(q in Question,
      where: q.order > ^minimal_order and q.form_id == ^testimony.form_id,
      order_by: :order
    )
    |> Repo.all()
    |> Enum.filter(fn q ->
      case Map.get(conditions, q.id) do
        nil ->
          true

        question_conditions ->
          question_conditions
          |> Enum.any?(fn choice ->
            Enum.all?(choice, fn c ->
              Enum.member?(choices, c)
            end)
          end)
      end
    end)
    |> List.first()
  end

  def get_by_key(key) do
    __MODULE__
    |> join(:left, [t], f in assoc(t, :form))
    |> join(:left, [t, f], q in assoc(f, :questions))
    |> where([t, f, q], t.key == ^key)
    |> preload([t, f, q], form: {f, questions: q})
    |> Repo.one()
  end

  def get_answers(testimony, question_id) do
    Answer
    |> where([a], a.testimony_id == ^testimony.id and a.question_id == ^question_id)
    |> Repo.all()
  end

  def insert_choices(question_id, testimony_id, value, choices_ids) do
    value =
      cond do
        is_integer(value) -> Integer.to_string(value)
        true -> value
      end

    {:ok, answer} =
      %Answer{
        testimony_id: testimony_id,
        question_id: question_id,
        value: value
      }
      |> Repo.insert(returning: true)

    choices =
      choices_ids
      |> Enum.map(fn cid ->
        %{
          answer_id: answer.id,
          question_choice_id: cid
        }
      end)

    Repo.insert_all(AnswerQuestionChoice, choices)
  end

  def upsert_choices(question_id, testimony_id, value, choices_ids, delete \\ true) do
    value =
      cond do
        is_integer(value) -> Integer.to_string(value)
        true -> value
      end

    # TODO
    {:ok, answer} =
      %Answer{
        testimony_id: testimony_id,
        question_id: question_id,
        value: value
      }
      |> Map.from_struct()
      |> Repo.upsert(Answer)

    choices_ids
    |> Enum.each(fn cid ->
      %AnswerQuestionChoice{
        answer_id: answer.id,
        question_choice_id: cid
      }
      |> Map.from_struct()
      |> Repo.upsert(AnswerQuestionChoice)
    end)

    # Delete not choseen answers but already present from previous submits
    if delete do
      from(
        a in AnswerQuestionChoice,
        where: a.answer_id == ^answer.id and a.question_choice_id not in ^choices_ids
      )
      |> Repo.delete_all()
    end
  end
end
