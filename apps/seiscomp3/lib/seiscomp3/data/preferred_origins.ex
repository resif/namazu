defmodule Seiscomp3.Data.PreferredOrigins do
  @behaviour Seiscomp3.Syncer

  require Logger

  import Ecto.Query

  alias Namazu.Seismic.Origin
  alias Namazu.Repo

  def state_key, do: "preferred_origin"

  def query do
    from(
      e in "event",
      join: pe in "publicobject",
      on: [_oid: e._oid],
      join: po in "publicobject",
      on: [m_publicid: e.m_preferredoriginid],
      join: o in "origin",
      on: [_oid: po._oid],
      order_by: [e._last_modified, e._oid],
      select: %{
        origin: %{
          time:
            fragment(
              "(?||'.'||to_char(?, 'fm000000'))::timestamp with time zone",
              o.m_time_value,
              o.m_time_value_ms
            ),
          latitude: o.m_latitude_value,
          longitude: o.m_longitude_value,
          depth: o.m_depth_value,
          depth_type: o.m_depthtype,
          origin_type: o.m_type,
          automatic: o.m_evaluationmode == "automatic"
        },
        last_modified: e._last_modified,
        event_oid: e._oid
      }
    )
  end

  def import_entry(entry) do
    unless Namazu.Repo.in_transaction?(), do: raise("Can't be run outside a transaction")

    last_modified = Seiscomp3.Time.cast_time(entry.last_modified)
    mapping = Namazu.Repo.get_by(Seiscomp3.Mapping, event_oid: entry.event_oid)

    case mapping do
      nil ->
        {:missing_match, %Seiscomp3.Mapping{event_oid: entry.event_oid}}

      mapping ->
        origin =
          entry.origin
          |> Seiscomp3.Data.Origins.prepare()
          |> Map.put(:event_id, mapping.event_id)
          |> Repo.match(Origin)

        case origin do
          nil ->
            {:missing_match, entry.origin}

          origin ->
            if not origin.event_preferred do
              Logger.info("Set Origin ##{origin.id} as preferred for Event ##{mapping.event_id})")

              origin
              |> Origin.changeset(%{event_preferred: true})
              |> Repo.update!()
            end

            {:ok, origin, last_modified, entry.event_oid}
        end
    end
  end
end
