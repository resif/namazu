defmodule Namazu.Macro.AnswerQuestionChoice do
  use Ecto.Schema
  import Ecto.Changeset

  @schema_prefix :macro

  schema "answers_questions_choices" do
    belongs_to(:answer, Namazu.Macro.Answer)
    belongs_to(:question_choice, Namazu.Macro.QuestionChoice)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(answer_question_choice, params \\ %{}) do
    answer_question_choice
    |> cast(params, [:answer_id, :question_choice_id])
  end

  def distinctive_fields() do
    [:answer_id, :question_choice_id]
  end
end
