defmodule Namazu do
  use Timex

  require Logger

  @moduledoc """
  Documentation for Namazu.
  """

  @key_ignore_list [
    :__struct__,
    :__meta__,
    :id,
    :origin_id,
    :created_at,
    :inserted_at,
    :updated_at
  ]

  def local_datetime(datetime, timezone) do
    timex_timezone = Timezone.get(timezone)
    Timezone.convert(datetime, timex_timezone)
  end

  def to_string(entry) do
    id =
      case Map.get(entry, :id) do
        nil ->
          Map.get(entry, :origin_id)

        id ->
          id
      end

    name =
      case Map.has_key?(entry, :__struct__) do
        true ->
          entry.__struct__
          |> Atom.to_string()
          |> String.split(".")
          |> List.last()

        false ->
          nil
      end

    values =
      entry
      |> Map.keys()
      |> Enum.filter(fn key -> key not in @key_ignore_list end)
      |> Enum.map_join("/", fn key ->
        value = Map.get(entry, key)

        value_str =
          case value do
            %{__cardinality__: _} ->
              "<not_printed>"

            %{__calendar__: _} ->
              DateTime.to_string(value)

            _ ->
              value
          end

        "#{key}:#{value_str}"
      end)

    case [name, id] do
      [nil, nil] ->
        "[#{values}]"

      [name, id] ->
        "#{name}##{id}[#{values}]"
    end
  end

  def get_parameter(key) do
    Namazu.Parameter
    |> Namazu.Repo.get(key)
    |> Map.get(:value)
  end
end
