defmodule Namazu.Macro.QuestionConditionChoice do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :macro

  schema "questions_conditions_choices" do
    belongs_to(:question_choice, Namazu.Macro.QuestionChoice)
    belongs_to(:question_condition, Namazu.Macro.QuestionCondition)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(question_condition_choice, params \\ %{}) do
    question_condition_choice
    |> cast(params, [:question_condition_id, :question_choice_id])
  end

  def distinctive_fields() do
    [:question_choice_id, :question_condition_id]
  end
end
