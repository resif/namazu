defmodule Namazu.Instruments.Station do
  use Ecto.Schema
  import Ecto.Changeset

  @schema_prefix :instruments

  schema "stations" do
    belongs_to(:network, Namazu.Instruments.Network)
    belongs_to(:actor, Namazu.Actor)
    has_many(:periods, Namazu.Instruments.StationPeriod)

    field(:station_code, :string)
    field(:description, :string)
    field(:hidden, :boolean)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(station, params \\ %{}) do
    station
    |> cast(params, [:network_id, :station_code, :description])
    |> validate_required(:network_id)
  end

  def distinctive_fields() do
    [:network_id, :station_code]
  end
end
