defmodule Seiscomp3.Mixfile do
  use Mix.Project

  def project do
    [
      app: :seiscomp3,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Seiscomp3.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [{:namazu, in_umbrella: true}] ++ NamazuProject.Mixfile.deps()
  end
end
