#!/bin/sh

docker run \
  -it --rm \
  -p 127.0.0.1:5432:5432 \
  -e POSTGRES_USER=namazu \
  -v ~/Databases/namazu:/var/lib/postgresql/data \
  --name namazu-db \
  namazu-db \
  postgres -c log_min_duration_statement=1 -c shared_buffers=8GB
