defmodule Mix.Tasks.Namazu.SetParameter do
  use Mix.Task

  alias Namazu.Repo

  @shortdoc "Set a parameter"

  def run(args) do
    Mix.Task.run("app.start")

    arguments = [locale: :string, slug: :string, title: :string]

    {_options, args, _} = OptionParser.parse(args, strict: arguments)

    [key, value] = args

    %{
      key: key,
      value: value
    }
    |> IO.inspect()
    |> Repo.upsert(Namazu.Parameter)
  end
end
