defmodule Namazu.Macro.QuestionGroup do
  use Ecto.Schema

  import Ecto.Changeset
  alias Namazu.Macro.Question

  @schema_prefix :macro

  schema "questions_groups" do
    belongs_to(:form, Namazu.Macro.Form)

    has_many(:questions, Question, foreign_key: :group_id)

    field(:text, :string)
    # field(:subtext, :string)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def changeset(question, params \\ %{}) do
    question
    |> cast(params, [:form_id, :text])
  end

  def distinctive_fields() do
    [:form_id, :text]
  end
end
