defmodule Namazu.Geography.City do
  use Ecto.Schema

  @schema_prefix :views

  schema "current_cities" do
    field(:country_iso2, :string)
    field(:administrative_code_type, :string)
    field(:administrative_code, :string)
    field(:postal_codes, {:array, :string})
    field(:source, :string)
    field(:source_version, :string)
    field(:source_id, :string)
    field(:name, :string)
    field(:population, :integer)
    field(:latitude, :float)
    field(:longitude, :float)
    field(:location, Geo.PostGIS.Geometry)
  end
end
