defmodule Namazu.Macro.Answer do
  use Ecto.Schema
  import Ecto.Changeset

  @schema_prefix :macro

  schema "answers" do
    belongs_to(:question, Namazu.Macro.Question)
    belongs_to(:testimony, Namazu.Macro.Testimony)

    has_many(:question_choices, Namazu.Macro.AnswerQuestionChoice)

    field(:value, :string)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:question_id, :testimony_id]
  end

  def changeset(answer, params \\ %{}) do
    answer
    |> cast(params, [:question_id, :testimony_id, :value])
  end
end
