CREATE TYPE macro.address_source AS ENUM (
  'gps',
  'witness'
);

CREATE TYPE macro.answer_type AS ENUM (
  'integer',
  'string',
  'text',
  'image',
  'level'
);

CREATE TYPE macro.form_type AS ENUM (
  'city',
  'individual'
);

CREATE TABLE IF NOT EXISTS macro.forms (
  id serial PRIMARY KEY,

  name varchar NOT NULL,
  "type" macro.form_type NOT NULL,
  "default" boolean NOT NULL DEFAULT FALSE,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE UNIQUE INDEX unique_default_form ON macro.forms(type, "default") WHERE "default";
ALTER TABLE namazu.zones ADD CONSTRAINT zones_individual_form_id_key FOREIGN KEY (individual_form_id) REFERENCES macro.forms(id);
ALTER TABLE namazu.zones ADD CONSTRAINT zones_city_form_id_key FOREIGN KEY (city_form_id) REFERENCES macro.forms(id);

CREATE TABLE IF NOT EXISTS macro.questions_groups (
  id serial PRIMARY KEY,
  form_id integer NOT NULL REFERENCES macro.forms(id) ON DELETE CASCADE,

  text varchar NOT NULL,
  subtext varchar,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX questions_groups_form_id_idx ON macro.questions_groups(form_id);

CREATE TABLE IF NOT EXISTS macro.questions (
  id serial PRIMARY KEY,
  form_id integer NOT NULL REFERENCES macro.forms(id) ON DELETE CASCADE,
  group_id integer REFERENCES macro.questions_groups(id),

  answer_type macro.answer_type NOT NULL,
  multiple boolean NOT NULL DEFAULT FALSE,
  "order" smallint NOT NULL,
  required boolean NOT NULL DEFAULT FALSE,
  type varchar, -- Enum date, people, objects, environment, animals, buildings
  text varchar NOT NULL,
  subtext varchar,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE(form_id, "order")
);

CREATE TABLE IF NOT EXISTS macro.questions_choices (
  id serial PRIMARY KEY,
  question_id integer NOT NULL REFERENCES macro.questions(id) ON DELETE CASCADE,

  value varchar,
  url varchar,
  "order" smallint NOT NULL,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE(question_id, "order")
);

CREATE TABLE IF NOT EXISTS macro.questions_conditions (
  id serial PRIMARY KEY,

  question_id integer NOT NULL REFERENCES macro.questions(id) ON DELETE CASCADE,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX questions_conditions_question_id_idx ON macro.questions_conditions(question_id);

CREATE TABLE IF NOT EXISTS macro.questions_conditions_choices (
  id serial PRIMARY KEY,
  question_choice_id integer NOT NULL REFERENCES macro.questions_choices(id),
  question_condition_id integer NOT NULL REFERENCES macro.questions_conditions(id),

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE(question_choice_id, question_condition_id)
);

CREATE INDEX questions_conditions_choices_question_condition_id_idx ON macro.questions_conditions_choices(question_condition_id);

CREATE TABLE IF NOT EXISTS macro.surveys (
  id serial PRIMARY KEY,
  event_id integer REFERENCES seismic.events(id) NOT NULL,

  key uuid NOT NULL UNIQUE DEFAULT gen_random_uuid(),
  name varchar,
  opened boolean NOT NULL DEFAULT false,
  "type" macro.form_type NOT NULL DEFAULT 'city', -- Need improvment

  created_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX surveys_event_id_idx ON macro.surveys(event_id);

CREATE TABLE IF NOT EXISTS macro.testimonies (
  id serial PRIMARY KEY,
  city_id integer REFERENCES geography.areas(id),
  event_id integer REFERENCES seismic.events(id),
  form_id integer REFERENCES macro.forms(id),

  key uuid NOT NULL UNIQUE,
  felt_time timestamp with time zone,
  naive_felt_time timestamp without time zone,
  timezone character varying(30),
  highlighted boolean NOT NULL DEFAULT FALSE,
  utc_offset smallint,
  quality double precision, -- Enum poor, very poor, great
  evaluation_status macro.evaluation_status,
  user_evaluation_status macro.evaluation_status,
  author varchar,
  email varchar,
  organization varchar,
  source varchar,
  address varchar,
  address_source macro.address_source,
  postal_code varchar(10),
  administrative_code_type varchar(10),
  administrative_code varchar(10),
  expert_comment text,
  geocoding_type varchar,
  latitude double precision,
  longitude double precision,
  location geometry(Point, 4326),

  created_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX testimonies_city_id_idx ON macro.testimonies(city_id);
CREATE INDEX testimonies_event_id_idx ON macro.testimonies(event_id);
CREATE INDEX testimonies_form_id_idx ON macro.testimonies(form_id);

CREATE TABLE IF NOT EXISTS macro.answers (
  id serial PRIMARY KEY,
  question_id integer NOT NULL REFERENCES macro.questions(id) ON DELETE CASCADE,
  testimony_id integer NOT NULL REFERENCES macro.testimonies(id) ON DELETE CASCADE,

  value varchar,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX answers_question_id_idx ON macro.answers(question_id);
CREATE INDEX answers_testimony_id_idx ON macro.answers(testimony_id);

-- CREATE UNIQUE INDEX intensities_unique_city_preferred_idx ON macro.intensities(event_id, city_id) WHERE city_preferred;

CREATE TABLE IF NOT EXISTS macro.answers_questions_choices (
  id serial PRIMARY KEY,
  answer_id integer NOT NULL REFERENCES macro.answers(id) ON DELETE CASCADE,
  question_choice_id integer NOT NULL REFERENCES macro.questions_choices(id),

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX answers_questions_choices_answer_id_idx ON macro.answers_questions_choices(answer_id);
CREATE INDEX answers_questions_choices_question_choice_id_idx ON macro.answers_questions_choices(question_choice_id);

CREATE TABLE IF NOT EXISTS macro.intensities (
  id serial PRIMARY KEY,
  event_id integer REFERENCES seismic.events(id),
  testimony_id integer REFERENCES macro.testimonies(id),
  testimony_preferred boolean DEFAULT False,
  intensity double precision NOT NULL,
  intensity_lower_uncertainty double precision,
  intensity_upper_uncertainty double precision,
  intensity_type varchar NOT NULL, -- enum 'EMS-98', 'MSK' ?
  method varchar NOT NULL, -- enum 'thumbnail', 'corrected thumbnail' ?
  evaluation_status varchar, -- enum 'final', 'preliminary' NOT NULL ?
  quality double precision,
  automatic boolean NOT NULL,
  author varchar, -- public.authors ?
  notes text,
  city_id integer REFERENCES geography.areas(id),
  city_preferred boolean DEFAULT False,
  city_administrative_code_type varchar(10),
  city_administrative_code varchar(10),
  latitude double precision,
  longitude double precision,
  location geometry(Point, 4326),

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone
);

CREATE INDEX intensities_city_id_idx ON macro.intensities(city_id);
CREATE INDEX intensities_event_id_idx ON macro.intensities(event_id);
CREATE INDEX intensities_testimony_id_idx ON macro.intensities(testimony_id);

CREATE UNIQUE INDEX intensities_unique_city_preferred_idx ON macro.intensities(event_id, city_id) WHERE city_preferred;
CREATE UNIQUE INDEX intensities_unique_testimony_preferred_idx ON macro.intensities(event_id, testimony_id) WHERE testimony_preferred AND testimony_id IS NOT NULL;

CREATE TABLE IF NOT EXISTS macro.intensities_contributions (
  id serial PRIMARY KEY,
  parent_intensity_id integer NOT NULL REFERENCES macro.intensities(id),
  intensity_id integer NOT NULL REFERENCES macro.intensities(id),
  weight double precision,

  inserted_at timestamp with time zone NOT NULL DEFAULT current_timestamp,
  updated_at timestamp with time zone,

  UNIQUE (parent_intensity_id, intensity_id)
);

CREATE INDEX intensities_contributions_intensity_id_idx ON macro.intensities_contributions(intensity_id);
