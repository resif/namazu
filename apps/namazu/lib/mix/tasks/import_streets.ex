defmodule Mix.Tasks.Namazu.ImportStreets do
  use Mix.Task
  use Timex

  alias Namazu.Geography
  alias Namazu.Repo
  alias Namazu.Helpers

  @shortdoc "Import streets data from a CSV file"

  def get_street(data) when is_list(data) and length(data) == 10 do
    [
      source_id,
      postal_code,
      insee_code,
      longitude,
      latitude,
      minimal_longitude,
      maximal_longitude,
      minimal_latitude,
      maximal_latitude,
      name
    ] = data

    %Geography.Street{
      source_id: source_id,
      administrative_code_type: "INSEE",
      administrative_code: insee_code,
      postal_code: postal_code,
      longitude: Helpers.parse_float(longitude),
      latitude: Helpers.parse_float(latitude),
      minimal_longitude: Helpers.parse_float(minimal_longitude),
      maximal_longitude: Helpers.parse_float(maximal_longitude),
      minimal_latitude: Helpers.parse_float(minimal_latitude),
      maximal_latitude: Helpers.parse_float(maximal_latitude),
      name: name
    }
  end

  def get_street(_data) do
    nil
  end

  def run(args) do
    Mix.Task.run("app.start")

    {_options, file, _} = OptionParser.parse(args, strict: [])
    # {options, file, _} = OptionParser.parse(args, strict: [exclude_country_iso2: :string])

    # excluded_country_iso2 = Keyword.get(options, :exclude_country_iso2, "") |> String.split(",")

    File.stream!(file)
    |> Stream.map(&String.trim/1)
    # |> Stream.with_index()
    |> Stream.map(fn line -> String.split(line, ";") end)
    |> Stream.map(&get_street/1)
    |> Stream.filter(fn data -> not is_nil(data) end)
    |> Stream.map(&Map.from_struct/1)
    |> Stream.map(fn data -> Repo.upsert(data, Geography.Street) end)
    |> Stream.run()
  end
end
