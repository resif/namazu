CREATE TRIGGER areas_set_updated_time
  BEFORE UPDATE ON geography.areas
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER areas_periods_set_updated_time
  BEFORE UPDATE ON geography.areas_periods
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER areas_set_period
  BEFORE INSERT OR UPDATE ON geography.areas_periods
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_period();

CREATE TRIGGER areas_set_geoms
  BEFORE INSERT OR UPDATE ON geography.areas_periods
  FOR EACH ROW EXECUTE PROCEDURE geography.set_geoms();

CREATE TRIGGER streets_set_location
  BEFORE INSERT OR UPDATE ON geography.streets
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_location();

CREATE TRIGGER timezones_set_updated_time
  BEFORE UPDATE ON geography.timezones
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER namazu_zones_set_boundary
  BEFORE INSERT OR UPDATE ON namazu.zones
  FOR EACH ROW EXECUTE PROCEDURE namazu.zone_set_boundary();

CREATE TRIGGER namazu_zones_areas_set_boundary
  AFTER INSERT OR UPDATE OR DELETE ON namazu.zones_areas
  FOR EACH ROW EXECUTE PROCEDURE namazu.zone_area_set_boundary();

CREATE TRIGGER events_01_set_updated_time
  BEFORE UPDATE ON seismic.events
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER events_02_set_hidden
  AFTER INSERT OR UPDATE ON seismic.events
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_hidden();

CREATE TRIGGER streams_set_updated_time
  BEFORE UPDATE ON seismic.streams
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER picks_set_updated_time
  BEFORE UPDATE ON seismic.picks
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER origins_00_set_updated_time
  BEFORE UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER origins_01_set_location
  BEFORE INSERT OR UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_location();

CREATE TRIGGER origins_02_set_event_origin_preferred
  BEFORE INSERT OR UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_event_origin_preferred();

CREATE TRIGGER origins_03_set_timezone
  BEFORE INSERT OR UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_event_timezone();

CREATE TRIGGER origins_04_manage_merged_event
  BEFORE UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE seismic.manage_merged_event();

CREATE TRIGGER origins_05_set_distintive_city
  BEFORE INSERT OR UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_event_distinctive_city();

CREATE TRIGGER origins_06_set_event_zone
  BEFORE INSERT OR UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_event_zone();

CREATE TRIGGER origins_07_set_event_hidden
  AFTER INSERT OR UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_event_hidden();

CREATE TRIGGER origins_08_set_event_publicid
  AFTER INSERT OR UPDATE ON seismic.origins
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_event_publicid();

CREATE TRIGGER origins_qualities_set_updated_time
  BEFORE UPDATE ON seismic.origins_qualities
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER origins_uncertainties_set_updated_time
  BEFORE UPDATE ON seismic.origins_uncertainties
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER arrivals_set_updated_time
  BEFORE UPDATE ON seismic.arrivals
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER amplitudes_set_updated_time
  BEFORE UPDATE ON seismic.amplitudes
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER stationmagnitudes_set_updated_time
  BEFORE UPDATE ON seismic.stationmagnitudes
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER magnitudes_01_set_event_magnitude_preferred
  BEFORE INSERT OR UPDATE ON seismic.magnitudes
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_event_magnitude_preferred();

CREATE TRIGGER magnitudes_02_set_updated_time
  BEFORE UPDATE ON seismic.magnitudes
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER magnitudes_03_set_event_hidden
  AFTER INSERT OR UPDATE ON seismic.magnitudes
  FOR EACH ROW EXECUTE PROCEDURE seismic.set_event_hidden();

CREATE TRIGGER stationmagnitudes_contributions_set_updated_time
  BEFORE UPDATE ON seismic.stationmagnitudes_contributions
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER forms_set_updated_time
  BEFORE UPDATE ON macro.forms
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER questions_set_updated_time
  BEFORE UPDATE ON macro.questions
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER questions_choices_set_updated_time
  BEFORE UPDATE ON macro.questions_choices
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER questions_conditions_set_updated_time
  BEFORE UPDATE ON macro.questions_conditions
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER answers_questions_choices_set_updated_time
  BEFORE UPDATE ON macro.answers_questions_choices
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER answers_questions_choices_delete_invalid_answers
  AFTER UPDATE OR DELETE ON macro.answers_questions_choices
  FOR EACH ROW
  EXECUTE PROCEDURE macro.delete_non_valid_answers();

CREATE TRIGGER surveys_set_updated_time
  BEFORE UPDATE ON macro.surveys
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER testimonies_01_set_location
  BEFORE INSERT OR UPDATE ON macro.testimonies
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_location();

CREATE TRIGGER testimonies_02_set_timezone
  BEFORE INSERT OR UPDATE ON macro.testimonies
  FOR EACH ROW EXECUTE PROCEDURE macro.set_testimony_timezone();

CREATE TRIGGER testimonies_03_set_felt_time
  BEFORE INSERT OR UPDATE ON macro.testimonies
  FOR EACH ROW
  EXECUTE PROCEDURE macro.set_testimony_felt_time();

CREATE TRIGGER testimonies_04_set_updated_time
  BEFORE UPDATE ON macro.testimonies
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER answers_set_updated_time
  BEFORE UPDATE ON macro.answers
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER intensities_set_location
  BEFORE INSERT OR UPDATE ON macro.intensities
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_location();

CREATE TRIGGER intensities_set_updated_time
  BEFORE UPDATE ON macro.intensities
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER intensities_contributions_set_updated_time
  BEFORE UPDATE ON macro.intensities_contributions
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER networks_set_updated_time
  BEFORE UPDATE ON instruments.networks
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER stations_set_updated_time
  BEFORE UPDATE ON instruments.stations
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();

CREATE TRIGGER stations_set_location
  BEFORE INSERT OR UPDATE ON instruments.stations_periods
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_location();

CREATE TRIGGER stations_set_period
  BEFORE INSERT OR UPDATE ON instruments.stations_periods
  FOR EACH ROW EXECUTE PROCEDURE namazu.set_period();

CREATE TRIGGER stations_periods_set_updated_time
  BEFORE UPDATE ON instruments.stations_periods
  FOR EACH ROW
  EXECUTE PROCEDURE namazu.set_updated_time();
