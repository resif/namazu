defmodule Namazu.Macro.IndividualIntensity do
  use Ecto.Schema

  @schema_prefix :macro

  schema "individual_intensities" do
    belongs_to(:testimony, Namazu.Macro.Testimony)

    field(:method, :string)
    field(:evaluation_status, :string)
    field(:automatic, :boolean)
    field(:quality, :string)
    field(:author, :string)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end
end
