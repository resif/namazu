defmodule Web.API.V1.EventController do
  use Web, :controller

  alias Namazu.Helpers
  alias Namazu.Cache

  def cities(conn, _params = %{"event_publicid" => event_publicid}) do
    cities =
      Cache.get_distintive_cities(event_publicid)
      |> Enum.map(&Helpers.to_feature(&1))
      |> Helpers.geojson()

    json(conn, cities)
  end

  def past_seismicity(conn, _params = %{"event_publicid" => event_publicid}) do
    events =
      Cache.get_past_seismicity(event_publicid)
      |> Enum.map(&Helpers.to_feature(&1))
      |> Helpers.geojson()

    json(conn, events)
  end

  def format_phase(phase) do
    %{
      arrivalID: phase.id,
      originID: phase.origin_id,
      pickID: phase.pick.id,
      networkCode: phase.pick.stream.network_code,
      stationCode: phase.pick.stream.station_code,
      locationCode: phase.pick.stream.location_code,
      channelCode: phase.pick.stream.channel_code,
      azimuth: phase.azimuth,
      distance: phase.distance,
      phaseCode: phase.phase_code,
      time: phase.pick.time,
      timeResidual: phase.time_residual,
      automatic: phase.pick.automatic
    }
  end

  def phases(conn, _params = %{"event_publicid" => event_publicid}) do
    phases =
      Cache.get_preferred_origin_phases(event_publicid)
      |> Enum.map(&Web.API.V1.EventController.format_phase(&1))

    json(conn, phases)
  end

  def types(conn, _params) do
    types =
      Cache.get_event_types()
      |> Enum.map(fn type ->
        %{name: type.event_type, count: type.count}
      end)

    json(conn, types)
  end

  def format_origin(origin) do
    %{
      id: origin.id,
      eventID: origin.event_id,
      eventPreferred: origin.event_preferred,
      time: origin.time,
      timeUncertainty: origin.time_uncertainty,
      timeConfidenceLevel: origin.time_confidence_level,
      latitude: origin.latitude,
      latitudeUncertainty: origin.latitude_uncertainty,
      latitudeConfidenceLevel: origin.latitude_confidence_level,
      longitude: origin.longitude,
      longitudeUncertainty: origin.longitude_uncertainty,
      longitudeConfidenceLevel: origin.longitude_confidence_level,
      depth: origin.depth,
      depthUncertainty: origin.depth_uncertainty,
      depthUsed: origin.depth_used,
      depthType: origin.depth_type,
      methodID: origin.method_id,
      earthmodelID: origin.earthmodel_id,
      automatic: origin.automatic,
      evaluationStatus: origin.evaluation_status,
      originType: origin.origin_type,
      agency: origin.agency,
      author: origin.author,
      createdAt: origin.created_at,
      insertedAt: origin.inserted_at,
      updatedAt: origin.updated_at
    }
  end

  def format_magnitude(magnitude) do
    %{
      id: magnitude.id,
      eventID: magnitude.event_id,
      originID: magnitude.origin_id,
      eventPreferred: magnitude.event_preferred,
      magnitude: magnitude.magnitude,
      magnitudeUncertainty: magnitude.magnitude_uncertainty,
      magnitudeType: magnitude.magnitude_type,
      methodID: magnitude.method_id,
      stationCount: magnitude.station_count,
      evaluationStatus: magnitude.evaluation_status,
      agency: magnitude.agency,
      author: magnitude.author,
      createdAt: magnitude.created_at,
      insertedAt: magnitude.inserted_at,
      updatedAt: magnitude.updated_at
    }
  end

  def format_event(event) do
    %{
      id: event.id,
      zoneID: event.zone_id,
      publicid: event.publicid,
      eventType: event.event_type,
      localtime: event.localtime,
      timezone: event.timezone,
      felt: event.felt,
      preferredOrigin: format_origin(event.preferred_origin),
      preferredMagnitude: format_magnitude(event.preferred_magnitude),
      insertedAt: event.inserted_at,
      updatedAt: event.updated_at
    }
  end

  def show(conn, _params = %{"event_publicid" => event_publicid}) do
    event =
      Cache.get_event(event_publicid)
      |> format_event()

    json(conn, event)
  end

  def index(conn, params) do
    search = %Namazu.Search.EventSearch{
      start_time: params["start_time"],
      end_time: params["end_time"],
      minimal_longitude: Helpers.parse_float(params["minimal_longitude"]),
      maximal_longitude: Helpers.parse_float(params["maximal_longitude"]),
      minimal_latitude: Helpers.parse_float(params["minimal_latitude"]),
      maximal_latitude: Helpers.parse_float(params["maximal_latitude"]),
      minimal_magnitude: Helpers.parse_float(params["minimal_magnitude"]),
      maximal_magnitude: Helpers.parse_float(params["maximal_magnitude"]),
      minimal_depth: Helpers.parse_float(params["minimal_depth"]),
      maximal_depth: Helpers.parse_float(params["maximal_depth"]),
      event_types: String.split(params["event_types"], ","),
      limit: Helpers.parse_integer(params["limit"]),
      offset: Helpers.parse_integer(params["offset"]),
      order_by: Helpers.parse_order_by(params["order_by"])
    }

    events =
      Namazu.Search.EventSearch.query(search)
      |> Namazu.Repo.all()
      |> Enum.map(&Helpers.to_feature(&1))
      |> Helpers.geojson()

    json(conn, events)
  end
end
