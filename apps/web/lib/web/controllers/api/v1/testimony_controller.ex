defmodule Web.API.V1.TestimonyController do
  import Plug.Conn

  use Web, :controller

  import Ecto.Query

  alias Namazu.Mailer
  alias Namazu.Macro.Testimony
  alias Namazu.Zone
  alias Namazu.Repo

  defp get_testimony_base_url() do
    Application.get_env(:web, Web)[:testimony_base_url]
  end

  # TODO
  # @allowed_update_parameters %{:latitude, :longitude}

  def get_form_id_by_event("individual", nil) do
    __MODULE__
    |> Repo.get_by(default: true, type: "individual")
  end

  def get_form_id_by_event("city", nil) do
    __MODULE__
    |> Repo.get_by(default: true, type: "city")
  end

  def get_form_id_by_event(type, event) do
    if is_nil(event.zone_id) do
      nil
    else
      Zone
      |> Repo.get(event.zone_id)
      |> Map.get(String.to_atom("#{type}_form_id"))
    end
  end

  def get_default_form_id(type) do
    Namazu.Macro.Form
    |> where(default: true, type: ^type)
    |> Repo.one()
    |> Map.get(:id)
  end

  def format_testimony(testimony) do
    # TODO : avoid this query
    event = Namazu.Repo.get(Namazu.Seismic.Event, testimony.event_id)

    %{
      key: testimony.key,
      formID: testimony.form_id,
      eventPublicID: event.publicid,
      naiveFeltTime: testimony.naive_felt_time,
      feltTime: testimony.felt_time,
      timezone: testimony.timezone,
      utcOffset: testimony.utc_offset,
      quality: testimony.quality,
      userEvaluationStatus: testimony.user_evaluation_status,
      source: testimony.source,
      administrativeCodeType: testimony.administrative_code_type,
      administrativeCode: testimony.administrative_code,
      geocodingType: testimony.geocoding_type,
      longitude: testimony.longitude,
      latitude: testimony.latitude
    }
  end

  def creatable_or_updatable(event \\ nil, testimony, type \\ nil) do
    case {event, testimony, type} do
      {nil, nil, "individual"} ->
        false

      {nil, testimony, "individual"} ->
        DateTime.diff(DateTime.utc_now(), testimony.created_at, :day) <= 15

      {event, _, "individual"} ->
        DateTime.diff(DateTime.utc_now(), event.preferred_origin.time, :day) <= 15

      {event, _, "city"} ->
        survey = Namazu.Macro.Survey |> Repo.get_by!(event_id: event.id)

        if is_nil(survey) do
          false
        else
          survey.opened
        end

      _ ->
        false
    end
  end

  def testimony(conn, %{"key" => key}) do
    testimony = Testimony.get_by_key(key)

    json(conn, format_testimony(testimony))
  end

  def new(conn, params) do
    if not Map.has_key?(params, "event_publicid") and not Map.has_key?(params, "naive_felt_time") do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Missing parameter : event_publicid or naive_felt_time"})
    end

    # TODO : Move checks to individual functions

    if not Map.has_key?(params, "type") do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Missing parameter : type"})
    end

    type = Map.get(params, "type")

    if not Map.has_key?(params, "email") do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Missing parameter : email"})
    end

    email = Map.get(params, "email")
    author = Map.get(params, "author")
    organization = Map.get(params, "organization")
    city_id = Map.get(params, "city_id")
    administrative_code = Map.get(params, "administrative_code")
    administrative_code_type = Map.get(params, "administrative_code_type")

    if type not in ["individual", "city"] do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Wrong value for type parameter"})
    end

    event_publicid = Map.get(params, "event_publicid")

    event =
      if event_publicid do
        Namazu.Seismic.event_query()
        |> Namazu.Seismic.preload_preferred()
        |> Namazu.Repo.get_by(publicid: event_publicid)
      else
        nil
      end

    if not is_nil(event_publicid) and is_nil(event) do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Event not found : #{event_publicid}"})
    end

    if not creatable_or_updatable(event, nil, type) do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Event older than 15 days : #{event_publicid}"})
    end

    event_id =
      if is_nil(event) do
        nil
      else
        event.id
      end

    form_id = get_form_id_by_event(type, event) || get_default_form_id(type)

    {:ok, testimony} =
      %Testimony{
        key: Ecto.UUID.generate(),
        event_id: event_id,
        form_id: form_id,
        city_id: city_id,
        administrative_code: administrative_code,
        administrative_code_type: administrative_code_type,
        author: author,
        email: email,
        organization: organization
      }
      |> Repo.insert()

    Mailer.testimony_creation(type, event_publicid, testimony, get_testimony_base_url())

    json(conn, format_testimony(testimony))
  end

  def update(conn, params) do
    key = params["key"]

    params =
      Map.drop(params, [
        "key",
        "timezone",
        "event_id",
        "form_id",
        "highlighted",
        "utc_offset",
        "quality",
        "source",
        "expert_comment",
        "geocoding_type"
      ])

    {:ok, testimony} =
      Testimony
      |> Repo.get_by(key: key)
      |> Testimony.changeset(params)
      |> Repo.update()

    json(conn, format_testimony(testimony))
  end

  def format_answer(answer) do
    %{
      id: answer.id,
      question_id: answer.question_id,
      choices: Enum.map(answer.question_choices, fn c -> c.question_choice_id end),
      value: answer.value
    }
  end

  def answers(conn, %{"key" => key}) do
    testimony = Testimony.get_by_key(key)

    answers =
      Testimony.get_answers(testimony.id)
      |> Enum.map(fn a ->
        format_answer(a)
      end)

    json(conn, answers)
  end

  def update_answer(conn, params) do
    if not Map.has_key?(params, "question_id") do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Missing parameters : question_id"})
    end

    # Check if choices parameters is present
    if not Map.has_key?(params, "choices") do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Missing parameters : choices"})
    end

    # TODO : check if choices if not empty if question is required

    testimony = Testimony.get_by_key(params["key"])
    question_id = params["question_id"]

    # Check if the testimony exists
    if is_nil(testimony) do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Testimony #{params["key"]} doesn't exist"})
    end

    choice_ids = params["choices"]

    question = Namazu.Macro.Question |> Repo.get(question_id)

    form = Namazu.Macro.Form |> Repo.get(testimony.form_id)

    if question.form_id != form.id do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Invalid question"})
    end

    # TODO : check if conditions are fulfilled for the current question

    event =
      Namazu.Seismic.event_query()
      |> Namazu.Seismic.preload_preferred()
      |> Repo.get(testimony.event_id)

    if not creatable_or_updatable(event, testimony, form.type) do
      conn
      |> Plug.Conn.put_status(400)
      |> json(%{error: "Testimony can't be updated"})
    end

    value = params["value"]

    Testimony.upsert_choices(question_id, testimony.id, value, choice_ids)

    answers =
      Testimony.get_answers(testimony.id)
      |> Enum.map(fn a ->
        format_answer(a)
      end)

    json(conn, answers)
  end
end
