defmodule Namazu.Mailer do
  use GenServer
  use Swoosh.Mailer, otp_app: :namazu
  import Swoosh.Email
  require EEx

  EEx.function_from_file(
    :def,
    :testimony_creation_html_body,
    "lib/namazu/templates/testimony_creation_html_body.eex",
    [
      :type,
      :event_publicid,
      :testimony,
      :testimony_base_url
    ]
  )

  EEx.function_from_file(
    :def,
    :testimony_creation_text_body,
    "lib/namazu/templates/testimony_creation_text_body.eex",
    [
      :type,
      :event_publicid,
      :testimony,
      :testimony_base_url
    ]
  )

  def start_link(_) do
    GenServer.start_link(__MODULE__, %{}, name: __MODULE__)
  end

  def init(_) do
    {:ok, %{}}
  end

  def testimony_creation(type, event_publicid, testimony, testimony_base_url) do
    GenServer.cast(
      __MODULE__,
      {:testimony_creation, type, event_publicid, testimony, testimony_base_url}
    )
  end

  def handle_cast(
        {:testimony_creation, type, event_publicid, testimony, testimony_base_url},
        state
      ) do
    new()
    |> to(testimony.email)
    |> from({"BCSF-Rénass", "bcsf-renass@unistra.fr"})
    |> subject("FranceSéisme - Votre témoignage")
    |> html_body(
      __MODULE__.testimony_creation_html_body(
        type,
        event_publicid,
        testimony,
        testimony_base_url
      )
    )
    |> text_body(
      __MODULE__.testimony_creation_text_body(
        type,
        event_publicid,
        testimony,
        testimony_base_url
      )
    )
    |> deliver()

    {:noreply, state}
  end
end
