import Config

config :web, namespace: Web

config :web, :generators, context_app: :web

config :tailwind,
  version: "3.0.23",
  default: [
    args: ~w(
  --config=tailwind.config.js
  --input=css/app.css
  --output=../priv/static/assets/app.css
  ),
    cd: Path.expand("../assets", __DIR__)
  ]

config :esbuild,
  version: "0.14.0",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../../../deps", __DIR__)}
  ]

config :web, Web.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "erxvLI5TEvDupv85zVLZyIqIvVQknXRhB34j+gB2LVlVpPeMHrn6QRxWlgqnRoN2",
  render_errors: [view: Web.ErrorView, accepts: ~w(html json)],
  pubsub_server: Web.PubSub,
  live_view: [signing_salt: "cVaEjSgb5/Z20duT8Faf0KzYr1aRl6FF"]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
if Mix.env() == :dev do
  import_config "dev.exs"
end
