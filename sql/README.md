# Exclusive Economic Zone

```sh
ogr2ogr -f "PostgreSQL" PG:"host=127.0.0.1 user=namazu dbname=namazu" EEZ_land_v2_201410.shp  -nlt geometry -lco GEOMETRY_NAME=boundary -lco FID=id -lco SCHEMA=geography -nln exclusive_economic_zones -progress

echo 'ALTER TABLE geography.exclusive_economic_zones DROP COLUMN objectid;' | psql -h 127.0.0.1 -U namazu
echo 'ALTER TABLE geography.exclusive_economic_zones DROP COLUMN changes;' | psql -h 127.0.0.1 -U namazu
echo 'ALTER TABLE geography.exclusive_economic_zones DROP COLUMN shape_leng;' | psql -h 127.0.0.1 -U namazu
echo 'ALTER TABLE geography.exclusive_economic_zones DROP COLUMN shape_area;' | psql -h 127.0.0.1 -U namazu
echo 'ALTER TABLE geography.exclusive_economic_zones RENAME COLUMN iso_3digit TO code_iso3;' | psql -h 127.0.0.1 -U namazu
echo 'ALTER TABLE geography.exclusive_economic_zones RENAME COLUMN country TO country_name;' | psql -h 127.0.0.1 -U namazu

insert into geography.exclusive_economic_zones(code_iso3, country_name, boundary, buffer) (select code_iso3, country_name, st_buffer(boundary, 0.18) as boundary, 20 as buffer from geography.exclusive_economic_zones);

pg_dump -a -t geography.exclusive_economic_zones -d namazu -U namazu -h 127.0.0.1 > 0008_eez.sql
```

# Cities

First convert Natural Earth Data shapefile to GeoJSON :
```sh
ogr2ogr -f GeoJSON  /tmp/natural_earth_data.geojson ~/Downloads/ne_10m_populated_places.shp
pg_dump -a -t geography.cities -d namazu -U namazu -h 127.0.0.1 > sql/0024_cities.sql
```


# Cities (Julien)

To generate 0009_cities.sql, we used the following commands :
```sh
# ign data http://professionnels.ign.fr/adminexpress
wget -P ./$WDIR/dl/ https://wxs-telechargement.ign.fr/x02uy2aiwjo9bm8ce5plwqmr/telechargement/prepackage/ADMINEXPRESS-COG-PACK_2018-05-04\$ADMIN-EXPRESS-COG_1-1__SHP__FRA_2018-04-03/file/ADMIN-EXPRESS-COG_1-1__SHP__FRA_2018-04-03.7z

# geonames data
wget -P ./$WDIR/dl/ http://download.geonames.org/export/dump/allCountries.zip
wget -P ./$WDIR/dl/ http://download.geonames.org/export/dump/alternateNamesV2.zip

# ign processing
## CHEF_LIEU
for zone in {FR,D971,D972,D973,D974,D976}; do 7z e ./$WDIR/dl/ADMIN-EXPRESS-COG_1-1__SHP__FRA_2018-04-03.7z -o./$WDIR/ign/$zone/ ADMIN*/*/1_*/ADE-COG_1-1_SHP_*_$zone/CHEF_LIEU.*; done

ogr2ogr -f "PostgreSQL" PG:"host=$HOST user=$USER dbname=$DBNAME" ./$WDIR/ign/FR/CHEF_LIEU.shp -t_srs EPSG:4326 -nlt geometry -lco GEOMETRY_NAME=geom -lco FID=gid -nln geography.chef_lieu_fr -progress

for zone in {D971,D972,D973,D974,D976}; do ogr2ogr -f "PostgreSQL" PG:"host=$HOST user=$USER dbname=$DBNAME" ./$WDIR/ign/$zone/CHEF_LIEU.shp -append -t_srs EPSG:4326 -nlt geometry -nln geography.chef_lieu_fr -progress; done

for column in {ar,bn,de,en,es,fr,el,hi,hu,id,it,ja,ko,nl,pl,pt,ru,sv,tr,vi,zh}; do echo "ALTER TABLE geography.chef_lieu_fr ADD COLUMN name_$column VARCHAR(80);"; done | psql -h $HOST -U $USER -d $DBNAME

## COMMUNE
for zone in {FR,D971,D972,D973,D974,D976}; do 7z e ./$WDIR/dl/ADMIN-EXPRESS-COG_1-1__SHP__FRA_2018-04-03.7z -o./$WDIR/ign/$zone/ ADMIN*/*/1_*/ADE-COG_1-1_SHP_*_$zone/COMMUNE.*; done

ogr2ogr -f "PostgreSQL" PG:"host=$HOST user=$USER dbname=$DBNAME" ./$WDIR/ign/FR/COMMUNE.shp -t_srs EPSG:4326 -nlt geometry -lco GEOMETRY_NAME=geom -lco FID=gid -nln geography.com_fr -progress

for zone in {D971,D972,D973,D974,D976}; do ogr2ogr -f "PostgreSQL" PG:"host=$HOST user=$USER dbname=$DBNAME" ./$WDIR/ign/$zone/COMMUNE.shp -append -t_srs EPSG:4326 -nlt geometry -nln geography.com_fr -progress; done

# geonames processing
7z e ./$WDIR/dl/allCountries.zip -o./$WDIR/ allCountries.txt
7z e ./$WDIR/dl/alternateNamesV2.zip -o./$WDIR/ alternateNamesV2.txt

sed "s/[$]WDIR[$]/$WDIR/" ./cities/0001_geonames.sql | psql -h $HOST -U $USER -d $DBNAME

# UPDATE FIELDS FROM other_tags
echo '\i ./cities/0002_update.sql' | psql -h $HOST -U $USER -d $DBNAME

for column in {ar,bn,de,en,es,fr,el,hi,hu,id,it,ja,ko,nl,pl,pt,ru,sv,tr,vi,zh}; do echo "
UPDATE geography.cities SET name_$column = maj.alternate_name
FROM (SELECT au.geonameid,
au.alternate_name
FROM geography.alternate_unique au
WHERE au.isolanguage = '$column') maj
WHERE geography.cities.geonameid = maj.geonameid
AND geography.cities.name_$column IS NULL
;"; done | psql -h $HOST -U $USER -d $DBNAME

echo 'ALTER TABLE geography.cities DROP COLUMN geonameid;' | psql -h $HOST -U $USER -d $DBNAME

for column in {ar,bn,de,en,es,fr,el,hi,hu,id,it,ja,ko,nl,pl,pt,ru,sv,tr,vi,zh}; do echo "
WITH t1 AS (SELECT ge.name,
                   au.alternate_name,
                   ST_SetSRID(ST_MakePoint(ge.longitude, ge.latitude),4326) AS geom
            FROM geography.alternate_unique au
            INNER JOIN geography.geoname ge ON au.geonameid = ge.geonameid
            WHERE au.isolanguage = '$column')

UPDATE geography.cities SET name_$column = t1.alternate_name
FROM t1
WHERE ST_DWithin(geography.cities.location, t1.geom, 10000, false)
AND geography.cities.name = t1.name
AND geography.cities.name_$column IS NULL
;"; done | psql -h $HOST -U $USER -d $DBNAME

pg_dump -a -t geography.cities -d $DBNAME -U $USER -h $HOST > 0009_cities.sql
```

# Delete events without origins

```
DELETE FROM seismic.events e
WHERE NOT EXISTS (
  SELECT 1
  FROM seismic.origins o
  WHERE o.event_id = e.id
);
```
