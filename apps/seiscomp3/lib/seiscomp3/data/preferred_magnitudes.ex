defmodule Seiscomp3.Data.PreferredMagnitudes do
  @behaviour Seiscomp3.Syncer

  require Logger

  import Ecto.Query

  alias Namazu.Seismic.Origin
  alias Namazu.Seismic.Magnitude
  alias Namazu.Repo

  def state_key, do: "preferred_magnitude"

  def query do
    from(
      e in "event",
      join: pm in "publicobject",
      on: [m_publicid: e.m_preferredmagnitudeid],
      join: m in "magnitude",
      on: [_oid: pm._oid],
      join: po in "publicobject",
      on: [m_publicid: e.m_preferredoriginid],
      join: o in "origin",
      on: [_oid: po._oid],
      order_by: [e._last_modified, e._oid],
      select: %{
        origin: %{
          time:
            fragment(
              "(?||'.'||to_char(?, 'fm000000'))::timestamp with time zone",
              o.m_time_value,
              o.m_time_value_ms
            ),
          longitude: o.m_longitude_value,
          latitude: o.m_latitude_value,
          depth: o.m_depth_value,
          depth_type: o.m_depthtype,
          automatic: o.m_evaluationmode == "automatic",
          origin_type: o.m_type
        },
        magnitude: %{
          magnitude: m.m_magnitude_value,
          magnitude_uncertainty: m.m_magnitude_uncertainty,
          magnitude_type: m.m_type
        },
        event_oid: e._oid,
        last_modified: e._last_modified
      }
    )
  end

  def import_entry(entry) do
    unless Namazu.Repo.in_transaction?(), do: raise("Can't be run outside a transaction")

    last_modified = Seiscomp3.Time.cast_time(entry.last_modified)
    mapping = Namazu.Repo.get_by(Seiscomp3.Mapping, event_oid: entry.event_oid)

    case mapping do
      nil ->
        {:missing_match, %Seiscomp3.Mapping{event_oid: entry.event_oid}}

      mapping ->
        origin =
          entry.origin
          |> Seiscomp3.Data.Origins.prepare()
          |> Map.put(:event_id, mapping.event_id)
          |> Repo.match(Origin)

        case origin do
          nil ->
            {:missing_match, entry.origin}

          origin ->
            magnitude =
              entry.magnitude
              |> Map.put(:origin_id, origin.id)
              |> Repo.match(Magnitude)

            if not magnitude.event_preferred do
              Logger.info(
                "Set Magnitude ##{magnitude.id} as preferred for Event ##{mapping.event_id}"
              )

              magnitude
              |> Magnitude.changeset(%{event_preferred: true})
              |> Repo.update!()
            end

            {:ok, origin, last_modified, entry.event_oid}
        end
    end
  end
end
