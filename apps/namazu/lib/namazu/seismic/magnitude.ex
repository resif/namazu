defmodule Namazu.Seismic.Magnitude do
  use Ecto.Schema

  import Ecto.Changeset

  @schema_prefix :seismic

  schema "magnitudes" do
    belongs_to(:event, Namazu.Seismic.Event)
    belongs_to(:origin, Namazu.Seismic.Origin)
    has_many(:station_magnitude_contributions, Namazu.Seismic.StationMagnitudeContribution)

    field(:event_preferred, :boolean)
    field(:magnitude, :float)
    field(:magnitude_uncertainty, :float)
    field(:magnitude_type, :string)
    field(:method_id, :string)
    field(:station_count, :integer)
    field(:evaluation_status, :string)
    field(:agency, :string)
    field(:author, :string)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:origin_id, :magnitude, :magnitude_uncertainty, :magnitude_type]
  end

  def changeset(magnitude, params \\ %{}) do
    magnitude
    |> cast(params, [
      :event_id,
      :origin_id,
      :event_preferred,
      :magnitude,
      :magnitude_uncertainty,
      :magnitude_type,
      :method_id,
      :station_count,
      :evaluation_status,
      :agency,
      :author,
      :created_at
    ])
  end
end
