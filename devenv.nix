{ pkgs, ... }:

{
  process-managers.overmind.enable = true;
  process-managers.process-compose.enable = false;

  services.postgres = {
    enable = true;
    createDatabase = false;
    package = pkgs.postgresql_15;
    initialDatabases = [{ name = "namazu"; }];
    listen_addresses = "localhost";
    extensions = extensions: [
      extensions.postgis
    ];
    initialScript = ''
      CREATE ROLE namazu WITH SUPERUSER LOGIN;
      ALTER DATABASE namazu OWNER TO namazu;
      \c namazu
      \i sql/000_common.sql
      \i sql/010_geography.sql
      \i sql/020_namazu.sql
      \i sql/030_seismic.sql
      \i sql/040_seiscomp3.sql
      \i sql/050_macro.sql
      \i sql/060_instruments.sql
      \i sql/070_views.sql
      \i sql/080_functions.sql
      \i sql/090_triggers.sql
    '';
  };

  services.mailpit.enable = true;

  languages.elixir.enable = true;

  processes = {
    mix.exec = "PGHOST= mix phx.server";
  };

  enterShell = ''
    unset PGHOST
  '';

  dotenv.enable = true;
}
