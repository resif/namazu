defmodule Web.Router do
  use Web, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:put_root_layout, {Web.LayoutView, :root})
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
    plug(Web.Plugs.SetLocale, "en")
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  pipeline :fdsn do
    plug(:accepts, ["json", "text", "xml"])
  end

  scope "/", Web do
    pipe_through(:browser)
  end

  scope "/api/v1", Web.API.V1 do
    pipe_through(:api)

    get("/events", EventController, :index)
    get("/events/types", EventController, :types)
    get("/events/:event_publicid", EventController, :show)
    get("/events/:event_publicid/cities", EventController, :cities)
    get("/events/:event_publicid/past-seismicity", EventController, :past_seismicity)
    get("/events/:event_publicid/phases", EventController, :phases)
    # get("/events/:event_publicid/intensities", EventController, :intensities)
    # get("/events/:event_publicid/testimonies", EventController, :testimonies)

    get("/zones", ZoneController, :index)
    get("/zones/:zone_slug", ZoneController, :show)

    get("/zones/:zone_slug/stations", ZoneController, :stations)

    get("/stations", StationController, :index)

    get("/surveys/forms", SurveyController, :forms)
    get("/surveys/forms/:id", SurveyController, :form)

    get("/surveys/cities/:key", SurveyController, :city_survey)
    # put("/surveys/cities/:key", SurveyController, :update_city_survey)

    # get("/testimonies", TestimonyController, :testimonies)
    post("/testimonies", TestimonyController, :new)
    get("/testimonies/:key", TestimonyController, :testimony)
    put("/testimonies/:key", TestimonyController, :update)
    get("/testimonies/:key/answers", TestimonyController, :answers)
    post("/testimonies/:key/answers", TestimonyController, :update_answer)
    put("/testimonies/:key/answers", TestimonyController, :update_answer)

    get("/cities", GeographyController, :cities)
    get("/streets", GeographyController, :streets)
  end

  scope "/fdsnws/event", Web do
    pipe_through(:fdsn)

    get("/", FDSNEventController, :base)
    get("/1/", FDSNEventController, :base)
    get("/1/query", FDSNEventController, :query)
    get("/1/application.wadl", FDSNEventController, :wadl)
    get("/1/catalogs", FDSNEventController, :catalogs)
    get("/1/contributors", FDSNEventController, :contributors)
    get("/1/version", FDSNEventController, :version)
  end

  scope "/:locale", Web do
    pipe_through(:browser)

    get("/search", EventController, :search)
    get("/search/results", EventController, :search_results)
  end
end
