defmodule Namazu.Macro.Survey do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Namazu.Repo

  @schema_prefix :macro

  schema "surveys" do
    belongs_to(:event, Namazu.Seismic.Event)

    field(:key, :binary_id)
    field(:name, :string)
    field(:opened, :boolean)
    field(:type, :string)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:key]
  end

  def changeset(survey, params \\ %{}) do
    survey
    |> cast(params, [:name, :opened, :type])
  end

  def get_by_key(key) do
    __MODULE__
    |> join(:left, [s], e in assoc(s, :event))
    |> where([s, e], s.key == ^key)
    |> preload([s, e], event: e)
    |> Repo.one()
  end
end
