import Config

config :namazu, Namazu.Repo, types: Namazu.PostgresTypes

config :web, Namazu.Gettext, locales: ~w(en fr), default_locale: "en"
