defmodule Namazu.Geography.Area do
  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  @schema_prefix :geography

  schema "areas" do
    has_many(:periods, Namazu.Geography.AreaPeriod)

    field(:area_type, :string)
    field(:district, :boolean)

    field(:geoname_id, :integer)
    field(:geoname_name, :string)

    field(:country_iso2, :string)
    field(:country_iso3, :string)

    field(:administrative_code_type, :string)
    field(:administrative_code, :string)

    field(:postal_codes, {:array, :string})

    field(:source, :string)
    field(:source_version, :string)
    field(:source_id, :string)

    field(:created_at, :utc_datetime_usec)
    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    # [:area_type, :administrative_code_type, :administrative_code]
    [:area_type, :source, :source_id]
  end

  def changeset(country, params \\ %{}) do
    country
    |> cast(params, [
      :area_type,
      :district,
      :geoname_id,
      :geoname_name,
      :country_iso2,
      :country_iso3,
      :administrative_code_type,
      :administrative_code,
      :source,
      :source_version,
      :source_id
    ])
  end

  # select name, name_vector from geography.areas_periods where name_vector @@ unaccent(lower('charle:* & Mézières:*'))::tsquery;

  def get_current_cities() do
    from(a in Namazu.Geography.City)
  end

  def get_current_cities_from_fullts(search) do
    search =
      search
      |> String.split()
      |> Enum.map_join(" & ", fn s -> s <> ":*" end)

    # |> Enum.join(" & ")

    __MODULE__.get_current_cities()
    |> where([c], fragment("name_vector @@ unaccent(lower(?))::tsquery", ^search))
    |> order_by([c], c.name)
  end

  def get_current_cities_from_postal_code(postal_code) do
    __MODULE__.get_current_cities()
    |> where([c], ^postal_code in c.postal_codes)
    |> order_by([c], c.name)
  end

  def get_current_cities_from_administrative_code(administrative_code_type, administrative_code) do
    __MODULE__.get_current_cities()
    |> where([c], ^administrative_code_type == c.administrative_code_type)
    |> where([c], ^administrative_code == c.administrative_code)
    |> order_by([c], c.name)
  end
end
