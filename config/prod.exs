import Config

config :web, Web.Endpoint, cache_static_manifest: "priv/static/cache_manifest.json", server: true

config :namazu, :environment, :prod
