defmodule Namazu.Macro.QuestionCondition do
  use Ecto.Schema
  alias Namazu.Macro.{Question, QuestionConditionChoice}
  @schema_prefix :macro

  schema "questions_conditions" do
    belongs_to(:question, Question)
    has_many(:choices, QuestionConditionChoice)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  #
  # def changeset(question_condition, params \\ %{}) do
  #   question_condition
  #   |> cast(params, [:question_id, :question_choice_id])
  # end

  #
  # def distinctive_fields() do
  #   [
  #     :question_id,
  #     :question_choice_id
  #   ]
  # end
end
