UPDATE macro.forms
   SET "default" = true
   WHERE name IN ('Questionnaire individuel 11 - Hexagone', 'Questionnaire collectif 12 - Hexagone');

UPDATE namazu.zones SET individual_form_id = (SELECT id FROM macro.forms WHERE name = 'Questionnaire individuel 11 - Hexagone') WHERE slug = 'france';
UPDATE namazu.zones SET individual_form_id = (SELECT id FROM macro.forms WHERE name = 'Questionnaire individuel 11 - Hexagone') WHERE slug = 'monde';
UPDATE namazu.zones SET individual_form_id = (SELECT id FROM macro.forms WHERE name = 'Questionnaire individuel 11 - DOMTOM') WHERE slug = 'la-reunion';
UPDATE namazu.zones SET individual_form_id = (SELECT id FROM macro.forms WHERE name = 'Questionnaire individuel 11 - DOMTOM') WHERE slug = 'les-antilles';
UPDATE namazu.zones SET individual_form_id = (SELECT id FROM macro.forms WHERE name = 'Questionnaire individuel 11 - DOMTOM') WHERE slug = 'mayotte';

UPDATE namazu.zones SET city_form_id = (SELECT id FROM macro.forms WHERE name = 'Questionnaire collectif 12 - Hexagone') WHERE slug = 'france';
UPDATE namazu.zones SET city_form_id = (SELECT id FROM macro.forms WHERE name = 'Questionnaire collectif 12 - Hexagone') WHERE slug = 'monde';
UPDATE namazu.zones SET city_form_id = (SELECT id FROM macro.forms WHERE name = 'Questionnaire collectif 12 - DOMTOM') WHERE slug = 'la-reunion';
UPDATE namazu.zones SET city_form_id = (SELECT id FROM macro.forms WHERE name = 'Questionnaire collectif 12 - DOMTOM') WHERE slug = 'les-antilles';
UPDATE namazu.zones SET city_form_id = (SELECT id FROM macro.forms WHERE name = 'Questionnaire collectif 12 - DOMTOM') WHERE slug = 'mayotte';

SELECT id, name, slug, individual_form_id, city_form_id FROM namazu.zones;
