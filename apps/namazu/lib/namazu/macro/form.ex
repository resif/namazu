defmodule Namazu.Macro.Form do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query

  alias Namazu.Repo
  alias Namazu.Macro.{Question, QuestionCondition, QuestionGroup, Testimony}

  @schema_prefix :macro

  schema "forms" do
    field(:name, :string)
    field(:type, :string)
    field(:default, :boolean)

    has_many(:questions, Question)
    has_many(:groups, QuestionGroup)
    has_many(:testimonies, Testimony)

    field(:inserted_at, :utc_datetime_usec)
    field(:updated_at, :utc_datetime_usec)
  end

  def distinctive_fields() do
    [:name]
  end

  def changeset(form, params \\ %{}) do
    form
    |> cast(params, [:name, :type, :default])
  end

  def get(form_id) do
    from(f in __MODULE__,
      left_join: g in assoc(f, :groups),
      left_join: q in assoc(f, :questions),
      left_join: c in assoc(q, :question_choices),
      left_join: cd in assoc(q, :conditions),
      left_join: cdc in assoc(cd, :choices),
      where: f.id == ^form_id,
      preload: [groups: g, questions: {q, question_choices: c, conditions: {cd, choices: cdc}}]
    )
  end

  def get_questions(form_id) when is_number(form_id) do
    from(q in Question,
      left_join: c in assoc(q, :conditions),
      left_join: qc in assoc(q, :question_choices),
      where: q.form_id == ^form_id,
      preload: [conditions: c, question_choices: qc]
    )
  end

  def has_question?(form, question_id) do
    Enum.any?(form.questions, fn q -> q.id == question_id end)
  end

  def get_conditions(form_id) do
    # Should be rewritten by using array_agg
    from(qc in QuestionCondition,
      left_join: q in assoc(qc, :question),
      left_join: c in assoc(qc, :choices),
      where: q.form_id == ^form_id,
      preload: [question: q, choices: c]
    )
    |> Repo.all()
    |> Enum.group_by(fn qc -> qc.question_id end, fn q ->
      Enum.map(q.choices, fn c -> c.question_choice_id end)
    end)
  end
end
