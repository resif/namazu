DROP SCHEMA macro CASCADE;

UPDATE namazu.zones SET individual_form_id = null, city_form_id = null;

\i 000_common.sql
\i 050_macro.sql
\i 070_views.sql
\i 080_functions.sql
\i 090_triggers.sql
